import { useState } from "react";


interface FormElement {
    id: number;
    form: JSX.Element; // Ou tout autre type approprié pour votre formulaire
  }

export const Critere = () => {
    const [count, setCount] = useState(1);
    const [formElements, setFormElements] = useState<FormElement[]>([]);
    function addformDIL() {
        return (
            <div id={"addFBIL" + count} className="flex flex-col md:flex-row justify-between items-center mb-4">
                <div className="flex-1 mb-4 md:mb-0 md:mr-2">
                <label htmlFor="date" className="block text-sm font-medium text-gray-700">Date</label>
                <input type="date" id="date" className="w-full fortext" />
                </div>
                <div className="flex-1 mb-4 md:mb-0">
                <label htmlFor="nombreInvites" className="block text-sm font-medium text-gray-700">Nombre d`&apos;`invités</label>
                <input type="number" id="nombreInvites" className="w-full fortext" placeholder="0" />
                </div>
                <div className="flex-1 w-full md:ml-2">
                <label htmlFor="lieuPrestation" className="block text-sm font-medium text-gray-700">Lieu de la prestation</label>
                <input type="text" id="lieuPrestation" className="w-full fortext" placeholder="Paris, Lyon, Nantes..." />
                </div>
            </div>);}
    
    const handleAddForm = () => {
        const newFormElements = [
          ...formElements, 
          { id: count, form: addformDIL() }
        ];
        setFormElements(newFormElements);
        setCount(count + 1); // Incrémente le compteur pour un identifiant unique
    };
    
    const handleDeleteForm = (idToDelete: any) => {
    const filteredForms = formElements.filter(element => element.id !== idToDelete);
    setFormElements(filteredForms);
    };

    return (
        <>
        <div className="mb-4">
            <h1 className="text-xl font-bold text-gray-700">Critères de la prestation</h1>
        </div>
        <div className="grid grid-cols-1 md:grid-cols-2 gap-6">
            {/* Colonne de gauche */}
            <div>
            <label htmlFor="title" className="block text-sm font-medium text-gray-700">Titre de l`&apos;`annonce</label>
            <input
                type="text"
                id="title"
                className="mt-1 block w-full rounded-md  fortext"
                placeholder="Ecrivez un titre"
            />
            <label htmlFor="description" className="block text-sm font-medium text-gray-700 mt-4">Description</label>
            <textarea
                id="description"
                rows={4}
                className="mt-1 block w-full rounded-md bg-[#F9F8F8] "
                placeholder="Ajoutez une description"
            />
            </div>
            {/* Colonne de droite */}
            <div className="pl-[30%]">
            <div className="flex flex-wrap m-2">
                <div className="p-2">
                <div className="bg-white p-4 rounded-lg shadow-lg text-center">
                    <div className="mb-2">
                    {/* Icône de photo ici */}
                    <span className="inline-block p-3 rounded-full bg-red-100">
                    <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" strokeWidth={1.5} stroke="currentColor" className="w-6 h-6">
                        <path strokeLinecap="round" strokeLinejoin="round" d="M6.827 6.175A2.31 2.31 0 0 1 5.186 7.23c-.38.054-.757.112-1.134.175C2.999 7.58 2.25 8.507 2.25 9.574V18a2.25 2.25 0 0 0 2.25 2.25h15A2.25 2.25 0 0 0 21.75 18V9.574c0-1.067-.75-1.994-1.802-2.169a47.865 47.865 0 0 0-1.134-.175 2.31 2.31 0 0 1-1.64-1.055l-.822-1.316a2.192 2.192 0 0 0-1.736-1.039 48.774 48.774 0 0 0-5.232 0 2.192 2.192 0 0 0-1.736 1.039l-.821 1.316Z" />
                        <path strokeLinecap="round" strokeLinejoin="round" d="M16.5 12.75a4.5 4.5 0 1 1-9 0 4.5 4.5 0 0 1 9 0ZM18.75 10.5h.008v.008h-.008V10.5Z" />
                    </svg>
                    </span>
                    </div>
                    <button className="text-sm text-gray-600">Ajouter une photo</button>
                </div>
                </div>
                <div className="p-2">
                <div className="bg-white p-4 rounded-lg shadow-lg text-center">
                    <div className="mb-2">
                    {/* Icône de photo ici */}
                    <span className="inline-block p-3 rounded-full bg-red-100">
                    <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" strokeWidth={1.5} stroke="currentColor" className="w-6 h-6">
                        <path strokeLinecap="round" strokeLinejoin="round" d="M6.827 6.175A2.31 2.31 0 0 1 5.186 7.23c-.38.054-.757.112-1.134.175C2.999 7.58 2.25 8.507 2.25 9.574V18a2.25 2.25 0 0 0 2.25 2.25h15A2.25 2.25 0 0 0 21.75 18V9.574c0-1.067-.75-1.994-1.802-2.169a47.865 47.865 0 0 0-1.134-.175 2.31 2.31 0 0 1-1.64-1.055l-.822-1.316a2.192 2.192 0 0 0-1.736-1.039 48.774 48.774 0 0 0-5.232 0 2.192 2.192 0 0 0-1.736 1.039l-.821 1.316Z" />
                        <path strokeLinecap="round" strokeLinejoin="round" d="M16.5 12.75a4.5 4.5 0 1 1-9 0 4.5 4.5 0 0 1 9 0ZM18.75 10.5h.008v.008h-.008V10.5Z" />
                    </svg>
                    </span>
                    </div>
                    <button className="text-sm text-gray-600">Ajouter une photo</button>
                </div>
                </div>
                <div className="p-2">
                <div className="bg-white p-4 rounded-lg shadow-lg text-center">
                    <div className="mb-2">
                    {/* Icône de photo ici */}
                    <span className="inline-block p-3 rounded-full bg-red-100">
                    <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" strokeWidth={1.5} stroke="currentColor" className="w-6 h-6">
                        <path strokeLinecap="round" strokeLinejoin="round" d="M6.827 6.175A2.31 2.31 0 0 1 5.186 7.23c-.38.054-.757.112-1.134.175C2.999 7.58 2.25 8.507 2.25 9.574V18a2.25 2.25 0 0 0 2.25 2.25h15A2.25 2.25 0 0 0 21.75 18V9.574c0-1.067-.75-1.994-1.802-2.169a47.865 47.865 0 0 0-1.134-.175 2.31 2.31 0 0 1-1.64-1.055l-.822-1.316a2.192 2.192 0 0 0-1.736-1.039 48.774 48.774 0 0 0-5.232 0 2.192 2.192 0 0 0-1.736 1.039l-.821 1.316Z" />
                        <path strokeLinecap="round" strokeLinejoin="round" d="M16.5 12.75a4.5 4.5 0 1 1-9 0 4.5 4.5 0 0 1 9 0ZM18.75 10.5h.008v.008h-.008V10.5Z" />
                    </svg>
                    </span>
                    </div>
                    <button className="text-sm text-gray-600">Ajouter une photo</button>
                </div>
                </div>
                <div className="p-2">
                <div className="bg-white p-4 rounded-lg shadow-lg text-center">
                    <div className="mb-2">
                    {/* Icône de photo ici */}
                    <span className="inline-block p-3 rounded-full bg-red-100">
                    <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" strokeWidth={1.5} stroke="currentColor" className="w-6 h-6">
                        <path strokeLinecap="round" strokeLinejoin="round" d="M6.827 6.175A2.31 2.31 0 0 1 5.186 7.23c-.38.054-.757.112-1.134.175C2.999 7.58 2.25 8.507 2.25 9.574V18a2.25 2.25 0 0 0 2.25 2.25h15A2.25 2.25 0 0 0 21.75 18V9.574c0-1.067-.75-1.994-1.802-2.169a47.865 47.865 0 0 0-1.134-.175 2.31 2.31 0 0 1-1.64-1.055l-.822-1.316a2.192 2.192 0 0 0-1.736-1.039 48.774 48.774 0 0 0-5.232 0 2.192 2.192 0 0 0-1.736 1.039l-.821 1.316Z" />
                        <path strokeLinecap="round" strokeLinejoin="round" d="M16.5 12.75a4.5 4.5 0 1 1-9 0 4.5 4.5 0 0 1 9 0ZM18.75 10.5h.008v.008h-.008V10.5Z" />
                    </svg>

                    </span>
                    </div>
                    <button className="text-sm text-gray-600">Ajouter une photo</button>
                </div>
                </div>
                {/* Répétez pour les emplacements de photo 2, 3, 4... */}
            </div>
            </div>


        </div>

        <div className="">
                <div className=" mx-auto  rounded-md ">
                {/* Ligne pour le type de prestation et le nom */}
                <div className="flex flex-col md:flex-row justify-between items-center mb-4">
                    <div className="flex-1 mb-4 md:mb-0 md:mr-2">
                    <label htmlFor="typePrestation" className="block text-sm font-medium  ">Type de prestation</label>
                    <select id="typePrestation" className="w-full fortext">
                        {/* Options du select ici */}
                        <option> 1 </option>
                    </select>
                    </div>
                    <div className="flex-1 md:ml-2">
                    <label htmlFor="nomPrestation" className="block text-sm font-medium text-gray-700">Nom de la prestation</label>
                    <input type="text" id="nomPrestation" className="w-full mt-1 p-2 fortext" placeholder="Nom de la prestation" />
                    </div>
                </div>
                <div className="flex flex-col md:flex-row justify-between items-center mb-4">
                    <div className="flex-1 mb-4 md:mb-0 md:mr-2">
                    <label htmlFor="date" className="block text-sm font-medium text-gray-700">Date</label>
                    <input type="date" id="date" className="w-full fortext" />
                    </div>
                    <div className="flex-1 mb-4 md:mb-0">
                    <label htmlFor="nombreInvites" className="block text-sm font-medium text-gray-700">Nombre d`&apos;`invités</label>
                    <input type="number" id="nombreInvites" className="w-full fortext" placeholder="0" />
                    </div>
                    <div className="flex-1 w-full md:ml-2">
                    <label htmlFor="lieuPrestation" className="block text-sm font-medium text-gray-700">Lieu de la prestation</label>
                    <input type="text" id="lieuPrestation" className="w-full fortext" placeholder="Paris, Lyon, Nantes..." />
                    </div>
                </div>

                <div>
                {formElements.map((element) => (
                    <div key={element.id}>
                    {element.form}

                    <button
                    className="flex ml-[10px] items-center px-6 py-4 border border-transparent text-base font-medium rounded-md text-[#ac352c] bg-white shadow-sm hover:bg-gray-100 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-[#ac352c]"
                    //onClick={handleDelete}
                    onClick={() => handleDeleteForm(element.id)}
                >
                    Supprimer
                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="currentColor" className="ml-2 -mr-1 h-5 w-5">
                        <path fillRule="evenodd" d="M4.25 12a.75.75 0 0 1 .75-.75h14a.75.75 0 0 1 0 1.5H5a.75.75 0 0 1-.75-.75Z" clipRule="evenodd" />
                    </svg>
                </button>
                    </div>
                ))}
                <button
                    className="flex mt-[10px] items-center px-6 py-4 border border-transparent text-base font-medium rounded-md text-[#ac352c] bg-white shadow-sm hover:bg-gray-100 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-[#ac352c]"
                    onClick={handleAddForm}
                >
                    Ajouter
                    <svg className="ml-2 -mr-1 h-5 w-5" fill="currentColor" viewBox="0 0 20 20">
                    <path fillRule="evenodd" d="M10 5a1 1 0 01 1 1v3h3a1 1 0 110 2h-3v3a1 1 0 01-2 0v-3H5a1 1 0 110-2h3V6a1 1 0 011-1z" clipRule="evenodd" />
                    </svg>
                </button>
                </div>


                {/* Radio buttons pour la durée */}
                <div className="mb-4">
                    <span className="text-gray-700">La prestation se déroule-t-elle sur plusieurs jours ?</span>
                    <div className="mt-2">
                    <label className="inline-flex items-center mr-6">
                        <input type="radio" className="form-radio" name="multiDay" value="yes" />
                        <span className="ml-2">Oui</span>
                    </label>
                    <label className="inline-flex items-center">
                        <input type="radio" className="form-radio" name="multiDay" value="no" />
                        <span className="ml-2">Non</span>
                    </label>
                    </div>
                </div>

                {/* Boutons */}
                <div className="flex justify-between mb-4">
                    <button className="bg-yellow-400 text-white px-6 py-2 rounded-lg shadow-md">Enregistrer et quitter</button>
                    <button className="bg-gray-300 text-white px-6 py-2 rounded-lg shadow-md">Étape suivante</button>
                </div>


                <div className="bg-orange-100 border-l-4 border-orange-500 text-orange-700 p-4">
                <p className="font-bold">Informations</p>
                <p className="text-sm">
                Votre prestation étant déjà passé, votre gain peut être affecté pour les raisons suivantes :
                </p>
                <ul className="text-sm text-gray-600 list-disc pl-5  text-orange-700">
                    <li>Évolution de la grille tarifaire du prestataire</li>
                    <li>Évolution des conditions générales de ventes du prestataire</li>
                    <li>Changement de date de la prestation</li>
                    </ul>
            </div>
                </div>
            </div>
    </>
    );
}




