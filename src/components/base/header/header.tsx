"use client"
import { FC, useEffect, useState } from 'react';

import './style.css';
import { Form, Radio, Tooltip } from 'react-daisyui';
import RadioInput from '../../forms/radio';
import BoutonInput from '../../forms/buttton';
import InputText from '../../forms/input-text';
import { useRouter } from 'next/navigation';








const Header: FC<any> = () => {

    const [localisation, setLocalisation] = useState([]);
    const [date, setDate] = useState('');
    const [radio1, setRadio1] = useState('C');
    const router = useRouter();
    const handleSubmit = (e: any) => {
        e.preventDefault(); // Empêche le rechargement de la page
        const timestamp = dateToTimestamp(date);
        const searchParams = new URLSearchParams();
        if (InputValue1ID) searchParams.append('type', InputValue1ID.toString());
        if (inputValue2) searchParams.append('location', inputValue2);
        if (date) searchParams.append('date', timestamp.toString());
        searchParams.append('prestation', radio1);

        router.push(`/services/?${searchParams.toString()}`);
    };

    function dateToTimestamp(dateStr: string): number {
    const date = new Date(dateStr);
    return date.getTime();
    }

    const [inputValue, setInputValue] = useState('');
  
    const [InputValue1ID, setInputValue1ID] = useState<any>(0);

    
    const setTypePrestationCategorie = async (e: any) => {
      const value = e.target.value;
      setInputValue(value);
      const tokenLS = localStorage.getItem('authToken');
  
      if (value) {
        try {
          const likeCategoryData = { caractere: value };
          const response = await fetch(`http://localhost:3000/v1/category/search/caracteres`, {
            method: 'POST',
            body: JSON.stringify(likeCategoryData),
            headers: {
              'Content-Type': 'application/json',
            },
          });
          if (!response.ok) {
            throw new Error(`Erreur HTTP: ${response.status}`);
          }
          const data = await response.json();
          setCategories(data);
        } catch (error) {
          console.error('Erreur lors de la requête:', error);
        }
      } else {
        setCategories([]);
      }
    };
    const [inputValue2, setInputValue2] = useState('');
    const setTypePrestationLocalisation = async (e: any) => {
      const value = e.target.value;
      setInputValue2(value);
  
      if (value ) {
        try {
          const likeLocationData = { caractere: value };
          const response = await fetch(`http://localhost:3000/v1/duration-info-service/search/caracteres`, {
            method: 'POST',
            body: JSON.stringify(likeLocationData),
            headers: {
              'Content-Type': 'application/json',
            },
          });
  
          if (!response.ok) {
            throw new Error(`Erreur HTTP: ${response.status}`);
          }
  
          const data = await response.json();
          setLocalisation(data);
        } catch (error) {
          console.error('Erreur lors de la requête:', error);
        }
      } else {
        setLocalisation([]);
      }
    };



    const handleSuggestionClick = (suggestion: any) => {
        // Supposons que 'suggestion' est un objet et que vous voulez utiliser une propriété 'nom'
        setInputValue(suggestion.name);
        setInputValue1ID(suggestion.Categorie_id);
        setCategories([]);
    };

    const handleSuggestionClick2 = (suggestion: any) => {
        // Supposons que 'suggestion' est un objet et que vous voulez utiliser une propriété 'nom'
        setInputValue2(suggestion.place_of_service);
        setLocalisation([]);
    };

    const [categories, setCategories] = useState<any>([]);

 // console.log(categories)
  return (
    <>
    <section className='max-[951px]:hidden relative w-full height-slide bg-black font-bold'>
        <div className='absolute top-0 left-0 w-full h-full overflow-hidden'>
            <div className='header-img-slide header-img1-slide-delay'></div>
            <div className='header-img-slide header-img2-slide-delay'></div>
            <div className='header-img-slide header-img3-slide-delay'></div>
            <div className='header-img-slide header-img4-slide-delay'></div>
            <div className='header-img-slide header-img5-slide-delay'></div>
            <div className='header-img-slide header-img6-slide-delay'></div>
        </div>
        <div className='absolute top-0 left-0 z-10 w-full'>
                <p className='py-14 text-center font-bold text-5xl text-white'>Gérez vos évènements en toute simplicité !</p>
                <Form  autoComplete='off'  onSubmit={handleSubmit} className='w-10/12 mx-auto p-8 rounded-md' style={{
                    backgroundColor: 'rgba(255, 255, 255, 0.4)',

                }}>
                    <div className='mx-auto flex flex-column flex-nowrap flex-col'>
                       
                        <div className='flex justify-evenly mb-3 text-lg font-bold ' >
                        <div className='mb-3 flex text-center justify-center w-2/5 '>
                            <label htmlFor='r-c'>Cession de prestation</label>
                            <input
                            type="radio"
                            title="Cession de prestation"
                            id='r-c'
                            value="C"
                            onChange={e => setRadio1(e.target.value)}
                            className='w-8'
                            defaultChecked
                            name="radio1"
                            />
                            <span title="Cession de prestation" className='circle-red'>?</span>
                        </div>
                        <div className='mb-3 flex text-center justify-center w-2/5 '>
                        <label htmlFor='r-r' >Recherche de prestation</label>
                                        <input
                            type="radio"
                            title="Recherche de prestation"
                            id='r-r'
                            value="R"
                            onChange={e => setRadio1(e.target.value)}
                            className='w-8'
                            name="radio1"
                            />
                            <span title="Recherche de prestation" className='circle-red'>?</span>
                        </div>
                        </div>
                        

                        <div className='flex justify-center p-10'>
              
                            <div className='w-full'>
                                <div className='flex flex-col'>
                                <label htmlFor="beautiful-input" className="block text-gray-700 label-slide mb-2 text-[18px]">
                                    Type de prestation
                                </label>
                                <input
                                    id="beautiful-input"
                                    type="text"
                                    name="type_of_service"
                                    className="rounded-r-lg p-3 w-full border-2 opacity-100 mt-0.5"
                                    placeholder="Données"
                                    value={inputValue ?? ''}
                                    onChange={setTypePrestationCategorie}
                                />
                                {categories.length > 0 && (
                                    <ul className="absolute z-10  bg-white w-[15%] border border-gray-300 mt-1 rounded-md shadow-lg max-h-60 overflow-auto">
                                    {categories.map((suggestion: any, index: any) => (
                                        <li key={index} 
                                        className="p-2 hover:bg-gray-100 cursor-pointer"
                                        onClick={() => handleSuggestionClick(suggestion)}>
                                        {suggestion.name} {/* Assurez-vous que cela correspond à la structure de votre objet suggestion */}
                                        </li>
                                    ))}
                                    </ul>
                                )}
                                
                            </div>
                            </div>
                            <div className='w-full'>
                                <div className='flex flex-col'>
                                <label htmlFor="beautiful-input2" className="block text-gray-700 label-slide mb-2 text-[18px]">
                                Localisation
                                </label>
                                <input
                                    id="beautiful-input2"
                                    type="text"
                                    name='localisation'
                                    className="rounded-none p-3 w-full border-2 bg-white opacity-100 mt-0.5"
                                    placeholder="Données"
                                    value={inputValue2 ?? ''}

                                    onChange={e => setTypePrestationLocalisation(e)}
                                />
                                {localisation.length > 0 && (
                                    <ul className="absolute z-10  bg-white w-[15%] border border-gray-300 mt-1 rounded-md shadow-lg max-h-60 overflow-auto">
                                    {localisation.map((suggestion: any, index) => (
                                        <li key={index} 
                                        className="p-2 hover:bg-gray-100 cursor-pointer"
                                        onClick={() => handleSuggestionClick2(suggestion)}>
                                        {suggestion.place_of_service} 
                                        </li>
                                    ))}
                                    </ul>
                                )}
                                
                                </div>

                            </div>
                            <div className='w-full'>
                                <div className='flex flex-col'>
                                <label htmlFor="beautiful-input" className="block text-gray-700 label-slide mb-2 text-[18px]">
                                Date
                                </label>
                                <input
                                    id="beautiful-input"
                                    type="date"
                                    name='date'
                                    className="rounded-r-lg p-3 w-full border-2 bg-white opacity-100 mt-0.5"
                                    onChange={e => setDate(e.target.value)}
                                />
                                </div>

                            </div>
                        </div>

                        <div className='flex justify-center mt-3'>
                            <BoutonInput className="bg-[#f59f4d] w-[125px] text-white p-3 rounded-[10px] opacity-100" type="submit" value="Rechercher" />
                        </div>
                    </div>
                </Form>
        </div>
    </section>

    <section className='min-[952px]:hidden relative w-full height-slide-mobile  font-bold'>
        <div className='absolute top-0 left-0 z-10 w-full'>
                <Form className='w-10/12  mx-auto p-8 rounded-md'>
                    <div className='mx-auto flex flex-col'>
                        <div className='flex justify-center p-5'>
                        <div className="w-full relative">
                        <label htmlFor="beautiful-input" className="block text-gray-700 text-sm font-bold mb-2">
                            Type de prestation
                        </label>
                        <div className="flex items-center border-2 ">
                            <input
                            id="beautiful-input"
                            type="text"
                            className="p-5 w-full rounded-full"
                            placeholder="Données"
                            />
                            <button 
                            className="absolute right-3"
                            onClick={() => {/* logique de recherche ici */}}
                            >
                            <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" strokeWidth={1.5} stroke="currentColor" className="w-6 h-6 text-[orange]">
                                <path strokeLinecap="round" strokeLinejoin="round" d="m21 21-5.197-5.197m0 0A7.5 7.5 0 1 0 5.196 5.196a7.5 7.5 0 0 0 10.607 10.607Z" />
                            </svg>

                            </button>
                        </div>
                        </div>
                        </div>
                    </div>
                </Form>
                </div>
    </section>
    </>
  );
}

export default Header;