'use client'
import { JSX, SVGProps, useEffect, useState } from "react"
import './style.css';
import { useRouter, useSearchParams  } from "next/navigation";

  export default function PasswordRecover() {
    const [data, setData] = useState<any>({});
    const [error, setError] = useState<any>({});
    const [success, setSuccess] = useState<any>({});
    const router = useRouter();
    const searchParams = useSearchParams();


    useEffect(()=>{
      if(!searchParams.has('token')){

        router.push("/auth/login")
      }
    }, [])



    const handleSubmit = async (e: any) => {
    e.preventDefault();
    const dataToSubmit: any = data;
    let formErrors: any = {};
    let isValid = true;
    if(dataToSubmit.password == "" || dataToSubmit.password == undefined )
    { 
      isValid = false;
      formErrors['password'] = 'un mot de passe valide';
      setError(formErrors);
      return;
    }

    if(dataToSubmit.confirm_password == "" || dataToSubmit.confirm_password == undefined )
    { 
      isValid = false;
      formErrors['confirm_password'] = 'le mot de passe doit correspondre';
      setError(formErrors);
      return;
    }
    if (isValid) {
      try {
        delete dataToSubmit.confirm_password
        dataToSubmit.token = searchParams.get("token");
        const response = await fetch('http://localhost:3000/v1/auth/update-password-forgot', {
          method: 'POST',
          body: JSON.stringify(dataToSubmit),

          headers: {
            'Content-Type': 'application/json',
        },
          // Pas besoin de spécifier le content-type header
        });
        const resultat = await response.json();
        if (response.ok) {
          router.push('/auth/login');

        } else {
          const message = resultat.message;
          setError({email: message})
        }

      } catch (error) {
        console.error("Erreur lors de l'envoi du formulaire", error);
      }
    };
    }



    function handleDataForm(e: any){
      const { name, value } = e.target;
      setData({ ...data, [name]: value });
    };
    const [passwordConfirmVisible, setPasswordConfirmVisible] = useState(false);
  

  
    const togglePasswordConfirmVisibility = () => {
      setPasswordConfirmVisible(!passwordConfirmVisible);
    };

    const [passwordConfirmVisible2, setPasswordConfirmVisible2] = useState(false);
  

  
    const togglePasswordConfirmVisibility2 = () => {
      setPasswordConfirmVisible2(!passwordConfirmVisible2);
    };
    return (
      <>    
      <div className='session-form-hold'>
          <div className="bg-white flex justify-center rounded">
            <div className="position w-[10px]">
              <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="currentColor" className="w-10 h-10 pl-[10px] pt-[10px]">
                <path fillRule="evenodd" d="M20.25 12a.75.75 0 01-.75.75H6.31l5.47 5.47a.75.75 0 11-1.06 1.06l-6.75-6.75a.75.75 0 010-1.06l6.75-6.75a.75.75 0 111.06 1.06l-5.47 5.47H19.5a.75.75 0 01.75.75z" clipRule="evenodd" />
              </svg>
            </div>
            <div className='text-center p-[20px]'>
              <div className="flex justify-center">
                <img width="40%" src="/assets/logo.png" alt="Logo" className="img-logo pt-10" />
              </div>
            <div className="inline-block pb-[20px]">
              <p className="red-span"> Entrez votre adresse mail et vous recevrez un nouveau mot de passe</p>
            </div>

            <form onSubmit={handleSubmit}>
              <div className="text-left">
                <div className="sm:w-full relative">
                  <label className='font-bold'>Mot de passe</label>
                  <div className="relative">
                    <input 
                      type={passwordConfirmVisible ? 'text' : 'password'} 
                      name="password" 
                      className='fortext mb-[8px] pl-3 pr-10' 
                      onChange={handleDataForm}  
                      required
                      placeholder="Mot de passe "
                    />
                    <span 
                      onClick={togglePasswordConfirmVisibility} 
                      className="absolute inset-y-0 right-0 pr-3 flex items-center text-sm cursor-pointer">
                      {passwordConfirmVisible ? 
                        <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" strokeWidth={1.5} stroke="currentColor" className="w-6 h-6">
                          <path strokeLinecap="round" strokeLinejoin="round" d="M3.98 8.223A10.477 10.477 0 0 0 1.934 12C3.226 16.338 7.244 19.5 12 19.5c.993 0 1.953-.138 2.863-.395M6.228 6.228A10.451 10.451 0 0 1 12 4.5c4.756 0 8.773 3.162 10.065 7.498a10.522 10.522 0 0 1-4.293 5.774M6.228 6.228 3 3m3.228 3.228 3.65 3.65m7.894 7.894L21 21m-3.228-3.228-3.65-3.65m0 0a3 3 0 1 0-4.243-4.243m4.242 4.242L9.88 9.88" />
                        </svg>
                        : 
                        <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" strokeWidth={1.5} stroke="currentColor" className="w-6 h-6">
                          <path strokeLinecap="round" strokeLinejoin="round" d="M2.036 12.322a1.012 1.012 0 0 1 0-.639C3.423 7.51 7.36 4.5 12 4.5c4.638 0 8.573 3.007 9.963 7.178.07.207.07.431 0 .639C20.577 16.49 16.64 19.5 12 19.5c-4.638 0-8.573-3.007-9.963-7.178Z" />
                          <path strokeLinecap="round" strokeLinejoin="round" d="M15 12a3 3 0 1 1-6 0 3 3 0 0 1 6 0Z" />
                        </svg>
                      }
                    </span>
                  </div>
                  {error.password && <p className="form-error-msg">{error.password}</p>}
                </div>
                <div className="sm:w-full relative">
                  <label className='font-bold'>Confirmez mot de passe</label>
                  <div className="relative">
                    <input 
                      type={passwordConfirmVisible2 ? 'text' : 'password'} 
                      name="confirm_password" 
                      className='fortext mb-[8px] pl-3 pr-10' 
                      onChange={handleDataForm}  
                      required
                      placeholder="Mot de passe "
                    />
                    <span 
                      onClick={togglePasswordConfirmVisibility2} 
                      className="absolute inset-y-0 right-0 pr-3 flex items-center text-sm cursor-pointer">
                      {passwordConfirmVisible2 ? 
                        <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" strokeWidth={1.5} stroke="currentColor" className="w-6 h-6">
                          <path strokeLinecap="round" strokeLinejoin="round" d="M3.98 8.223A10.477 10.477 0 0 0 1.934 12C3.226 16.338 7.244 19.5 12 19.5c.993 0 1.953-.138 2.863-.395M6.228 6.228A10.451 10.451 0 0 1 12 4.5c4.756 0 8.773 3.162 10.065 7.498a10.522 10.522 0 0 1-4.293 5.774M6.228 6.228 3 3m3.228 3.228 3.65 3.65m7.894 7.894L21 21m-3.228-3.228-3.65-3.65m0 0a3 3 0 1 0-4.243-4.243m4.242 4.242L9.88 9.88" />
                        </svg>
                        : 
                        <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" strokeWidth={1.5} stroke="currentColor" className="w-6 h-6">
                          <path strokeLinecap="round" strokeLinejoin="round" d="M2.036 12.322a1.012 1.012 0 0 1 0-.639C3.423 7.51 7.36 4.5 12 4.5c4.638 0 8.573 3.007 9.963 7.178.07.207.07.431 0 .639C20.577 16.49 16.64 19.5 12 19.5c-4.638 0-8.573-3.007-9.963-7.178Z" />
                          <path strokeLinecap="round" strokeLinejoin="round" d="M15 12a3 3 0 1 1-6 0 3 3 0 0 1 6 0Z" />
                        </svg>
                      }
                    </span>
                  </div>
                  {error.confirm_password && <p className="error-message">{error.confirm_password}</p>}
                </div>
              </div>
              <input type="submit"  className="bg-[#f49f4d] rounded-[10px] w-[80px] p-[10px] m-[10px] text-white cursor-pointer custom-box-shadow " value="Envoyer" />

            </form>

          </div>
        </div>
      </div>

  </>
    )
  }