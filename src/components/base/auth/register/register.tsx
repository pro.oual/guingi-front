'use client'
import { useState, useEffect } from 'react';
import { Card} from '@mui/material';
import * as React from 'react';
import './style.css';
import { exit } from 'process';
import { useRouter } from 'next/navigation';
import { useAuth } from '@/src/libs/authContext';
enum Status {
  DisplayParticulier,
  DisplayPrestataire,
  NotDisplay,
}

export default function Register({value} : any) {
  const [isParticulier, setParticulier] = useState<Status>(Status.NotDisplay);
  const [ismobile, setMobile] = useState<boolean>(true);
  const [image, setImage] = useState<string>('/assets/user_rouge.png');
  const [data, setData] = useState<any>({});
  const [error, setError] = useState<any>({});
  const router = useRouter();


  const {verifyToken} = useAuth();

  useEffect(() => {
    const executeVerification = async () => {
      const response = await verifyToken();
      if (response && value === "!auth") {
        router.push("/");
      }
    };
    executeVerification();
  }, [value]);

  const handleSubmit = async (e: any) => {
  e.preventDefault();
  const dataToSubmit: any = data;
  let formErrors: any = {};
  let isValid = true;

  if(dataToSubmit.genre == "" || dataToSubmit.genre == undefined )
  { 
    isValid = false;
    formErrors['genre'] = 'selectionnez le sexe';
    setError(formErrors);
    return;
  }

  if(dataToSubmit.password !== dataToSubmit.confirm_password)
  { 
    isValid = false;
    formErrors['password'] = 'Les mots de passe ne correspondent pas';
    setError(formErrors);
    return;
  }

  if (!/^(?=.*[A-Z])(?=.*[a-z])(?=.*[!@#$%^&*]).{6,}$/.test(dataToSubmit.password)) {
    isValid = false;


    formErrors['password'] = 'Le mot de passe n\'est pas correct.';
    setError(formErrors);
    return;
  }

  if(!/^((\+|00)33\s?|0)[67](\s?\d{2}){4}$/.test(dataToSubmit.phone_number) )
  {
    isValid = false;
    formErrors['phone_number'] = 'Erreur de saisie';
    setError(formErrors);
    return;
  }

  if(!/^(?:(?:IT|SM)\d{2}[A-Z]\d{22}|CY\d{2}[A-Z]\d{23}|NL\d{2}[A-Z]{4}\d{10}|LV\d{2}[A-Z]{4}\d{13}|(?:BG|BH|GB|IE)\d{2}[A-Z]{4}\d{14}|GI\d{2}[A-Z]{4}\d{15}|RO\d{2}[A-Z]{4}\d{16}|KW\d{2}[A-Z]{4}\d{22}|MT\d{2}[A-Z]{4}\d{23}|NO\d{13}|(?:DK|FI|GL|FO)\d{16}|MK\d{17}|(?:AT|EE|KZ|LU|XK)\d{18}|(?:BA|HR|LI|CH|CR)\d{19}|(?:GE|DE|LT|ME|RS)\d{20}|IL\d{21}|(?:AD|CZ|ES|MD|SA)\d{22}|PT\d{23}|(?:BE|IS)\d{24}|(?:FR|MR|MC)\d{25}|(?:AL|DO|LB|PL)\d{26}|(?:AZ|HU)\d{27}|(?:GR|MU)\d{28})$/.test(dataToSubmit.iban)){
    isValid = false;
    formErrors['iban'] = 'Erreur de saisie';
    setError(formErrors);
    return;
  }




  if (isValid) {
    const formData = new FormData();
    if (typeof dataToSubmit["is_entreprise"] !== "boolean") {
      if (dataToSubmit["is_entreprise"] === "particulier") {
        dataToSubmit["is_entreprise"] = false;
      } else if (dataToSubmit["is_entreprise"] === "prestataire") {
        dataToSubmit["is_entreprise"] = true;
      }
    }
    if (dataToSubmit["mobilite_entreprise"] === "oui") {
      dataToSubmit["mobilite_entreprise"] = true;
    } 
    if (dataToSubmit["mobilite_entreprise"] === "non") {
      dataToSubmit["mobilite_entreprise"] = false;
    } 
    if (!dataToSubmit["general_conditions"]) {
      return;      
    } 
    Object.keys(dataToSubmit).forEach(key => {
        formData.append(key, dataToSubmit[key]);
    });
    
    if(formData.get('confirm_password')){
      formData.delete('confirm_password');
    }

    try {
      const response = await fetch('http://localhost:3000/v1/users', {
        method: 'POST',
        body: formData, // Pas besoin de spécifier le content-type header
      });
      const resultat = await response.json();
      if (response.ok) {
        router.push('/auth/login');
      } else {
        const message = resultat.message;
        alert(message)
      }

    } catch (error) {
      console.error("Erreur lors de l'envoi du formulaire", error);
    }
  };
  }

  const handleFileChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    const file = event.target.files?.[0];
    if (file) {
      if (file.type !== "image/jpeg" && file.type !== "image/jpg" && file.type !== "image/png") {
        setError({...error, avatar: "L'extension n'est pas bonne : png/jpg"});
      }
      if(file.size > 3145728 ){
        setError({...error,avatar:"le fichier et trop lourd max: 3mo"});
      }
      const { name, value } = event.target;
      setData({ ...data, [name]: file });
      const reader = new FileReader();
      reader.onloadend = () => {
        setImage(reader.result as string);
      };
      reader.readAsDataURL(file);
    }
  };

  function handleDataForm(e: any){
    if(e.target.name == "mobilite_entreprise" ){
      setMobile(!ismobile)
    }

    if(e.target.name == "is_entreprise"){
      switch(e.target.value){
        case 'particulier' : setParticulier(Status.DisplayParticulier);
        setError({})
        break;
        case 'prestataire' : setParticulier(Status.DisplayPrestataire);
        setError({})
        break;
      }
    }
    const { name, value } = e.target;
    setData({ ...data, [name]: value });
  };

  const [passwordVisible, setPasswordVisible] = useState(false);
  const [passwordConfirmVisible, setPasswordConfirmVisible] = useState(false);

  
  const togglePasswordVisibility = () => {
    setPasswordVisible(!passwordVisible);
  };

  const togglePasswordConfirmVisibility = () => {
    setPasswordConfirmVisible(!passwordConfirmVisible);
  };


  const [passwordVisiblePresta, setPasswordVisiblePresta] = useState(false);
  const [passwordConfirmVisiblePresta, setPasswordConfirmVisiblePresta] = useState(false);

  
  const togglePasswordVisibilityPresta = () => {
    setPasswordVisiblePresta(!passwordVisiblePresta);
  };

  const togglePasswordConfirmVisibilityPresta = () => {
    setPasswordConfirmVisiblePresta(!passwordConfirmVisiblePresta);
  };


  return (
      <>
        <div className='justify-center font-bold'>
          <div className='p-[5%]'>
            <Card className='drop-shadow-lg'>
            <form onSubmit={handleSubmit}  encType="multipart/form-data">

            <div className='flex p-[2%]'>
              <img src={image} alt="" className='w-[100px]' />
              <div className='inline'>
                <label className="custom-file-input-button">
                  <span>Charger une photo</span>
                  <input type="file"  name="avatar" 
                  className="bg-black text-white sm:w-full" 
                  onChange={handleFileChange}
                  />
                </label>
                {error.avatar && <p className="error-message">{error.avatar}</p>}

              </div>
            </div>
              <div className='pl-[2%] pr-[2%]'>
              <div>
                <label className="flex font-bold  asterix--after"  >Vous êtes :</label>
                <div className="inline">
                  <input type="radio" name="is_entreprise"   id="particulier" value="particulier" onChange={handleDataForm} className="m-[10px] " required />
                  <label htmlFor="particulier">Particulier</label>
                  <input type="radio" name="is_entreprise"   id="prestataire" value="prestataire" onChange={handleDataForm}  className="m-[10px] "/>
                  <label htmlFor="prestataire">Prestataire</label>
                </div>
              </div>
              <div className=''>
                {isParticulier === Status.DisplayParticulier ? (
                  <div className="infos">
                    <p>Informations personnelles</p> 
                  </div>
                ) : null}
                {isParticulier === Status.DisplayPrestataire ? (
                  <div className="infos">
                    <p>Informations prestataire</p>
                  </div>
                ) : null}
              </div>

              {isParticulier === Status.DisplayParticulier ? (
              <><div className=''>
             
              <div className="md:flex sm:flex-none w-full  px-[4px] pt-[10px] pb-[20px] justify-center gap-2 ">
              <div className=" w-full sm:w-full">
                <label className='asterix--after'>Civilité</label>
                <select className="fortext sm:w-full" name='genre' onChange={handleDataForm} required>
                  <option className='options-list' value="" >Choisissez une valeur</option>
                  <option className='options-list' value="Monsieur">Monsieur</option>
                  <option className='options-list' value="Madame" >Madame</option>
                </select>
                {error.genre && <p className="error-message">{error.genre}</p>}
              </div>
              <div className=" w-full ">
                <label className='asterix--after'>Nom</label>
                <input type='text' className='fortext' name="last_name" placeholder='Dupont' onChange={handleDataForm} required/>
              </div>
              <div className=" w-full ">
                <label className='asterix--after'>Prenom</label>
                <input type='text' className='fortext' name='first_name' placeholder='Jean' onChange={handleDataForm} required/>
              </div>
              <div className=" w-full ">
                <label className='asterix--after'>Nom d&#39;utilisateur</label>
                <input type='text' className='fortext mb-[8px]' name="username" placeholder='Utilisateur'  onChange={handleDataForm} required/>
                <div  className="informations ">
                  <img src="/assets/informations.png" />
                  <p>Seul votre nom d&#39;utilisateur sera rendu public</p>
                </div>
              </div>
              <div className=" w-full ">
                <label className='asterix--after'>Date de naissance</label>
                <input type='date' className='fortext' name="date_of_birth" onChange={handleDataForm} placeholder='' required />
              </div>
            </div>
             
          
            </div>
            <div className='md:flex w-full  px-[4px] pb-[20px] justify-center gap-2 '>
            <div className=" w-full ">
                <label className='asterix--after'>Adresse</label>
                <input type='text' className='fortext' name="address" onChange={handleDataForm} placeholder='103 rue' required/>
              </div>
              <div className=" w-full ">
                <label className='asterix--after'>Code postal</label>
                <input type='number' className='fortext' name="zip_code" onChange={handleDataForm} placeholder='59000' required/>
              </div>
              <div className=" w-full ">
                <label className='asterix--after'>Ville</label>
                <input type='text' className='fortext' name="city" onChange={handleDataForm} placeholder='Lille' required/>
              </div>
              <div className=" w-full ">
                <label className='asterix--after'>Pays</label>
                <input type='text' className='fortext' name="country" onChange={handleDataForm} placeholder='France' required/>
              </div>
              <div className=" w-full ">
                <label className='asterix--after'>Telephone</label>
                <input type='tel' className='fortext' name="phone_number" onChange={handleDataForm} placeholder='0768261548' required/>
                {error.phone_number && <p className="error-message">{error.phone_number}</p>}
              
              </div>

            </div>


            <div className='md:flex  px-[4px] gap-2 pb-[20px]'>
            <div className="  sm:w-full">
                <label className='asterix--after'>Email</label>
                <input type='email' className='fortext' name="email" onChange={handleDataForm} placeholder='dupont@gmail.com' required />
              </div>
              
              
              <div className=" sm:w-full relative">
                <label className='asterix--after'>Mot de passe</label>
                <div className="relative">
                  <input 
                    type={passwordVisible ? 'text' : 'password'} 
                    name="password" 
                    className='fortext mb-[8px] pl-3 pr-10' // Padding à droite pour le bouton
                    onChange={handleDataForm} 
                    required
                  />
                  <span 
                    onClick={togglePasswordVisibility} 
                    className="absolute inset-y-0 right-0 pr-3 flex items-center text-sm cursor-pointer">
                    {passwordVisible ? <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" strokeWidth={1.5} stroke="currentColor" className="w-6 h-6">
                      <path strokeLinecap="round" strokeLinejoin="round" d="M3.98 8.223A10.477 10.477 0 0 0 1.934 12C3.226 16.338 7.244 19.5 12 19.5c.993 0 1.953-.138 2.863-.395M6.228 6.228A10.451 10.451 0 0 1 12 4.5c4.756 0 8.773 3.162 10.065 7.498a10.522 10.522 0 0 1-4.293 5.774M6.228 6.228 3 3m3.228 3.228 3.65 3.65m7.894 7.894L21 21m-3.228-3.228-3.65-3.65m0 0a3 3 0 1 0-4.243-4.243m4.242 4.242L9.88 9.88" />
                    </svg>
                      : 
                    <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" strokeWidth={1.5} stroke="currentColor" className="w-6 h-6">
                      <path strokeLinecap="round" strokeLinejoin="round" d="M2.036 12.322a1.012 1.012 0 0 1 0-.639C3.423 7.51 7.36 4.5 12 4.5c4.638 0 8.573 3.007 9.963 7.178.07.207.07.431 0 .639C20.577 16.49 16.64 19.5 12 19.5c-4.638 0-8.573-3.007-9.963-7.178Z" />
                      <path strokeLinecap="round" strokeLinejoin="round" d="M15 12a3 3 0 1 1-6 0 3 3 0 0 1 6 0Z" />
                    </svg>
                    }
                  </span>
                </div>
                {error.password && <p className="error-message">{error.password}</p>}
                <p>Le mot de passe doit être composé <br />des éléments suivants : </p>
                <ul>
                  <li>6 caractères</li>
                  <li>Une majuscule</li>
                  <li>Une minuscule</li>
                  <li>Un caractère spécial</li>
                </ul>
              </div>

             <div className=" sm:w-full relative">
                  <label className='asterix--after'>Confirmez mot de passe</label>
                  <div className="relative">
                    <input 
                      type={passwordConfirmVisible ? 'text' : 'password'} 
                      name="confirm_password" 
                      className='fortext mb-[8px] pl-3 pr-10' 
                      onChange={handleDataForm}  
                      required
                    />
                    <span 
                      onClick={togglePasswordConfirmVisibility} 
                      className="absolute inset-y-0 right-0 pr-3 flex items-center text-sm cursor-pointer">
                      {passwordConfirmVisible ? 
                        <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" strokeWidth={1.5} stroke="currentColor" className="w-6 h-6">
                          <path strokeLinecap="round" strokeLinejoin="round" d="M3.98 8.223A10.477 10.477 0 0 0 1.934 12C3.226 16.338 7.244 19.5 12 19.5c.993 0 1.953-.138 2.863-.395M6.228 6.228A10.451 10.451 0 0 1 12 4.5c4.756 0 8.773 3.162 10.065 7.498a10.522 10.522 0 0 1-4.293 5.774M6.228 6.228 3 3m3.228 3.228 3.65 3.65m7.894 7.894L21 21m-3.228-3.228-3.65-3.65m0 0a3 3 0 1 0-4.243-4.243m4.242 4.242L9.88 9.88" />
                        </svg>
                        : 
                        <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" strokeWidth={1.5} stroke="currentColor" className="w-6 h-6">
                          <path strokeLinecap="round" strokeLinejoin="round" d="M2.036 12.322a1.012 1.012 0 0 1 0-.639C3.423 7.51 7.36 4.5 12 4.5c4.638 0 8.573 3.007 9.963 7.178.07.207.07.431 0 .639C20.577 16.49 16.64 19.5 12 19.5c-4.638 0-8.573-3.007-9.963-7.178Z" />
                          <path strokeLinecap="round" strokeLinejoin="round" d="M15 12a3 3 0 1 1-6 0 3 3 0 0 1 6 0Z" />
                        </svg>
                      }
                    </span>
                  </div>
                  {error.confirm_password && <p className="error-message">{error.confirm_password}</p>}
                </div>

            </div>
            <div className='md:flex px-[4px] pb-[20px] gap-2'>
            <div className=" w-full">
                <label className='asterix--after'>IBAN</label>
                <input type='texte' name="iban" className='fortext' onChange={handleDataForm} placeholder='FR28..' required/>
                {error.iban && <p className="error-message">{error.iban}</p>}
              
              </div>
              <div className=" w-full">
                <label className='asterix--after'>BIC</label>
                <input type='texte' name="bic" className='fortext' onChange={handleDataForm} placeholder='PSS...' required/>
              </div>
              <div className="w-full  ">
                <label className='asterix--after'>Domiciliation</label>
                <input type='texte' name="bank_name" className='fortext' onChange={handleDataForm} placeholder='Banque' required />
              </div>
            </div>
            <div className='flex w-full  px-[4px]  justify-center gap-2 '>

            <div className=" w-full asterix--after" >
            <input
              type="checkbox"
              id="switch"
              onChange={handleDataForm}
              name="general_conditions"
              value="true"
              required
              className="custom-checkbox"
            />
              <label>
                En cochant cette case, j&#39;accepte que mes données soient collectées uniquement par Guingï pour le fonctionnement de sa plateforme et à des fins marketing. J&#39;ai connaissance de la possibilité de faire valoir mon droit de retrait à tout moment.
              </label>
            </div>
            </div>
            <div className='flex justify-center p-[10px]' >
                <button type='submit' className='infos'>Envoyer</button>
            </div></>
                ) : null}
                {isParticulier  === Status.DisplayPrestataire ? (
                    <>
              <div className=''>
             
             <div className="md:flex sm:flex-none w-full  px-[4px] pt-[10px] pb-[20px] justify-center gap-2 ">
             <div className=" w-full customerStyle" >
                <label className='asterix--after'>Civilité</label>
                <select className="fortext" name='genre' onChange={handleDataForm} required>
                  <option className='options-list' value="" >Choisissez une valeur</option>
                  <option className='options-list' value="Monsieur" >Monsieur</option>
                  <option className='options-list' value="Madame">Madame</option>
                </select>
                {error.genre && <p className="error-message">{error.genre}</p>}

              </div>
             <div className=" w-full ">
               <label className='asterix--after'>Nom de l&#39;entreprise</label>
               <input required type='text' className='fortext' name="username" placeholder='Nom' onChange={handleDataForm}  />
               <div  className="informations ">
                 <img src="/assets/informations.png" />
                 <p>Seul votre nom d&#39;utilisateur sera rendu public</p>
               </div>
             </div>
             <div className=" w-full ">
               <label className='asterix--after'>Nom du gérant</label>
               <input required type='text' className='fortext' name="last_name" placeholder='Dupont' onChange={handleDataForm} />
             </div>
             <div className=" w-full ">
               <label className='asterix--after'>Prenom du gérant</label>
               <input required type='text' className='fortext mb-[8px]' name="first_name" placeholder='Jean' onChange={handleDataForm} />

             </div>
             <div className=" w-full ">
               <label className='asterix--after'>Siret</label>
               <input required type='text' className='fortext mb-[8px]' name="siret" placeholder='901 964 755 00025' onChange={handleDataForm} />
             </div>
             <div className=" w-full ">
                <label className='asterix--after'>Date de naissance</label>
                <input required type='date' className='fortext' name="date_of_birth" placeholder='' onChange={handleDataForm} />
              </div>
           </div>
           </div>
           <div className='md:flex px-[4px] gap-2 pb-[20px]'>
            <div className=" md:w-[60%] ">
               <label className='asterix--after'>Email</label>
               <input 
               required 
               type='email' className='fortext' name="email" onChange={handleDataForm} placeholder='dupont@gmail.com' />
             </div>
             <div className="md:w-[60%]">
                <label className='asterix--after'>Mot de passe</label>
                <div className="relative">
                  <input 
                    type={passwordVisiblePresta ? 'text' : 'password'} 
                    name="password" 
                    className='fortext mb-[8px] pl-3 pr-10' // Padding à droite pour le bouton
                    onChange={handleDataForm} 
                    required
                  />
                  <span 
                    onClick={togglePasswordVisibilityPresta} 
                    className="absolute inset-y-0 right-0 pr-3 flex items-center text-sm cursor-pointer">
                    {passwordVisiblePresta ? <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" strokeWidth={1.5} stroke="currentColor" className="w-6 h-6">
                      <path strokeLinecap="round" strokeLinejoin="round" d="M3.98 8.223A10.477 10.477 0 0 0 1.934 12C3.226 16.338 7.244 19.5 12 19.5c.993 0 1.953-.138 2.863-.395M6.228 6.228A10.451 10.451 0 0 1 12 4.5c4.756 0 8.773 3.162 10.065 7.498a10.522 10.522 0 0 1-4.293 5.774M6.228 6.228 3 3m3.228 3.228 3.65 3.65m7.894 7.894L21 21m-3.228-3.228-3.65-3.65m0 0a3 3 0 1 0-4.243-4.243m4.242 4.242L9.88 9.88" />
                    </svg>
                      : 
                    <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" strokeWidth={1.5} stroke="currentColor" className="w-6 h-6">
                      <path strokeLinecap="round" strokeLinejoin="round" d="M2.036 12.322a1.012 1.012 0 0 1 0-.639C3.423 7.51 7.36 4.5 12 4.5c4.638 0 8.573 3.007 9.963 7.178.07.207.07.431 0 .639C20.577 16.49 16.64 19.5 12 19.5c-4.638 0-8.573-3.007-9.963-7.178Z" />
                      <path strokeLinecap="round" strokeLinejoin="round" d="M15 12a3 3 0 1 1-6 0 3 3 0 0 1 6 0Z" />
                    </svg>
                    }
                  </span>
                </div>
                {error.password && <p className="error-message">{error.password}</p>}
                <p>Le mot de passe doit être composé <br />des éléments suivants : </p>
                <ul>
                  <li>6 caractères</li>
                  <li>Une majuscule</li>
                  <li>Une minuscule</li>
                  <li>Un caractère spécial</li>
                </ul>
              </div>

             <div className="md:w-[60%]">
                  <label className='asterix--after'>Confirmez mot de passe</label>
                  <div className="relative">
                    <input 
                      type={passwordConfirmVisiblePresta ? 'text' : 'password'} 
                      name="confirm_password" 
                      className='fortext mb-[8px] pl-3 pr-10' 
                      onChange={handleDataForm}  
                      required
                    />
                    <span 
                      onClick={togglePasswordConfirmVisibilityPresta} 
                      className="absolute inset-y-0 right-0 pr-3 flex items-center text-sm cursor-pointer">
                      {passwordConfirmVisiblePresta ? 
                        <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" strokeWidth={1.5} stroke="currentColor" className="w-6 h-6">
                          <path strokeLinecap="round" strokeLinejoin="round" d="M3.98 8.223A10.477 10.477 0 0 0 1.934 12C3.226 16.338 7.244 19.5 12 19.5c.993 0 1.953-.138 2.863-.395M6.228 6.228A10.451 10.451 0 0 1 12 4.5c4.756 0 8.773 3.162 10.065 7.498a10.522 10.522 0 0 1-4.293 5.774M6.228 6.228 3 3m3.228 3.228 3.65 3.65m7.894 7.894L21 21m-3.228-3.228-3.65-3.65m0 0a3 3 0 1 0-4.243-4.243m4.242 4.242L9.88 9.88" />
                        </svg>
                        : 
                        <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" strokeWidth={1.5} stroke="currentColor" className="w-6 h-6">
                          <path strokeLinecap="round" strokeLinejoin="round" d="M2.036 12.322a1.012 1.012 0 0 1 0-.639C3.423 7.51 7.36 4.5 12 4.5c4.638 0 8.573 3.007 9.963 7.178.07.207.07.431 0 .639C20.577 16.49 16.64 19.5 12 19.5c-4.638 0-8.573-3.007-9.963-7.178Z" />
                          <path strokeLinecap="round" strokeLinejoin="round" d="M15 12a3 3 0 1 1-6 0 3 3 0 0 1 6 0Z" />
                        </svg>
                      }
                    </span>
                  </div>
                  {error.confirm_password && <p className="error-message">{error.confirm_password}</p>}
                </div>



             <div className="md:w-[60%]  ">
               <label className='asterix--after'> Telephone</label>
               <input required type='tel' className='fortext' name="phone_number" placeholder='0768261548' onChange={handleDataForm} />
               {error.phone_number && <p className="error-message">{error.phone_number}</p>}
             
             </div>
             </div>
           <div className='md:flex  px-[4px] gap-2 pb-[20px]'>
             <div className=" w-full ">
               <label>Site internet</label>
               <input  type='text' className='fortext' name="website" placeholder='http://' onChange={handleDataForm} />
             </div>
             <div className=" w-full ">
               <label>Facebook</label>
               <input type='text' className='fortext' name="facebook" placeholder='@facebook'  onChange={handleDataForm} />
             </div>
             <div className=" w-full ">
               <label>Instagram</label>
               <input type='text' className='fortext' name="instagram" placeholder='@instagram' onChange={handleDataForm} />
             </div>
             <div className=" w-full ">
               <label>Pinterest</label>
               <input type='text' className='fortext' name="pinterest" placeholder='@pinterest' onChange={handleDataForm} />
             </div>
             <div className=" w-full ">
               <label>Youtube</label>
               <input type='text' className='fortext' name="youtube" placeholder='@chaine' onChange={handleDataForm} />
             </div>
           </div>
           <div className='md:flex px-[4px] pb-[20px] gap-2'>
           <div className=" w-full">
               <label className='asterix--after'>IBAN</label>
               <input required type='texte' className='fortext' name="iban" placeholder='FR28..'  onChange={handleDataForm} />
             </div>
             <div className=" w-full">
               <label className='asterix--after'>BIC</label>
               <input required type='texte' className='fortext' name="bic" placeholder='PSS...' onChange={handleDataForm} />
             </div>
             <div className="w-full  ">
               <label className='asterix--after'>Domiciliation</label>
               <input required type='texte' className='fortext' name="bank_name" placeholder='Banque' onChange={handleDataForm} />
             </div>
           </div>
           <div className='flex px-[4px] pb-[20px] gap-2'>
           <div className="infos">
              <p>Informations sur la prestation </p> 
            </div>
            </div>
            <div>
                <label className="font-bold asterix--after">Êtes-vous mobile?</label>
                <div className="inline">

                  <input required type="radio"  name="mobilite_entreprise" id="mobile-oui"  value="true" onChange={handleDataForm} className="border border-red-500 rounded p-2 m-[10px]" />
                  <label htmlFor="mobile-oui">Oui</label>
                  <input type="radio" name="mobilite_entreprise" id="mobile-non" value="false" onChange={handleDataForm}  className="border border-red-500 rounded p-2 m-[10px]"/>
                  <label htmlFor="mobile-non">Non</label>

                  <span title="Cession de prestation" className='circle-red p-[5px]' >?</span>

                </div>
              </div>
           <div className='w-full  px-[4px]  justify-center gap-2 '>
           <div className=''>
                {
                !ismobile ? (
      
                <div className=" w-full pb-[10px]">
                <label className='asterix--after' >Pays {ismobile}</label>
                <input required type='texte' className='fortext' name='country' placeholder='France' onChange={handleDataForm} />
                </div>
                ) : 
                <div className='md:flex w-full  px-[4px] pb-[20px] justify-center gap-2 '>
                <div className=" w-full ">
                    <label className='asterix--after'>Adresse</label>
                    <input required type='text' className='fortext' name='address' placeholder='103 rue' onChange={handleDataForm} />
                  </div>
                  <div className=" w-full ">
                    <label className='asterix--after'>Code postal</label>
                    <input required type='number' className='fortext' name='zip_code' placeholder='59000' onChange={handleDataForm} />
                  </div>
                  <div className=" w-full ">
                    <label className='asterix--after'>Ville</label>
                    <input required type='text' className='fortext' name='city' placeholder='Lille' onChange={handleDataForm} />
                  </div>
                  <div className=" w-full ">
                    <label className='asterix--after'>Pays</label>
                    <input required type='text' className='fortext' name='country' placeholder='France' onChange={handleDataForm} />
                  </div>
                </div>
                }
              </div>
            </div>
              <div className='flex w-full  px-[4px]  justify-center gap-2 '>
              <div className=" w-full">
              <input
                type="checkbox"
                onChange={handleDataForm}
                name='general_conditions'
                value="true"
                required
              className="custom-checkbox"

              />
              <label className='asterix--after'>
                En cochant cette case, j&#39;accepte que mes données soient collectées uniquement par Guingï pour le fonctionnement de sa plateforme et à des fins marketing. J&#39;ai connaissance de la possibilité de faire valoir mon droit de retrait à tout moment.
              </label>
              </div>
              </div>
              <div className='flex justify-center p-[10px]' >
                  <button type='submit' className='bg-[#fbd97c] rounded-[5px] p-[10px] w-[220px] text-center  font-bold'>Envoyer</button>
              </div>

       
              </>
          ) : null}
        </div>
                </form>
                </Card>
                <div>
              </div>
            </div>
          </div>
</>
  );
}
