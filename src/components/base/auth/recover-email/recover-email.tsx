'use client'

import { JSX, SVGProps, useState } from "react"
import './style.css';
import { useRouter } from "next/navigation";
import { useAuth } from "@/src/libs/authContext";
import Link from "next/link";

  export default function PasswordRecoverEmail() {
    const [data, setData] = useState<any>({});
    const [error, setError] = useState<any>({});
    const [success, setSuccess] = useState<any>({});
    const router = useRouter()
    
    const handleSubmit = async (e: any) => {
    e.preventDefault();
    const dataToSubmit: any = data;
    let formErrors: any = {};
    let isValid = true;


    if(dataToSubmit.email == "" || dataToSubmit.email == undefined )
    { 
      isValid = false;
      formErrors['email'] = 'Une adresse e-mail est requis';
      setError(formErrors);
      return;
    }

    if (isValid) {
      try {
        const response = await fetch('http://localhost:3000/v1/auth/send-verify-password', {
          method: 'POST',
          body: JSON.stringify(dataToSubmit),

          headers: {
            'Content-Type': 'application/json',
        },
          // Pas besoin de spécifier le content-type header
        });
        const resultat = await response.json();
        if (response.ok) {
          setSuccess({email:"courrier envoye avec succes"})
        }
      } catch (error) {
        console.error("Erreur lors de l'envoi du formulaire", error);
      }
    };
    }

    function handleDataForm(e: any){
      const { name, value } = e.target;
      setData({ ...data, [name]: value });
    };


    const  handleClick = (): void => {
        router.push('/auth/login')
    }

    return (
      <>    
      <div className='session-form-hold'>
          <div className="bg-white flex justify-center rounded p-[10px]">
            <div className="position w-[10px] cursor-pointer">

            <i className="cursor-pointer ml-[10px] mt-[10px]">
            <Link href="/auth/login">

              <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="currentColor" className="w-8 h-8">
                <path fillRule="evenodd" d="M20.25 12a.75.75 0 01-.75.75H6.31l5.47 5.47a.75.75 0 11-1.06 1.06l-6.75-6.75a.75.75 0 010-1.06l6.75-6.75a.75.75 0 111.06 1.06l-5.47 5.47H19.5a.75.75 0 01.75.75z" clipRule="evenodd" />
              </svg>
              
            </Link>

            </i>
            </div>
            <div className='text-center p-[20px]'>
              <div className="flex justify-center">
                <img width="40%" src="/assets/logo.png" alt="Logo" className="img-logo pt-10" />
              </div>
            <div className="inline-block pb-[20px]">
              <p className="red-span"> Entrez votre adresse mail et vous recevrez un nouveau mot de passe</p>
              {success.email && <p className="form-success-msg">{success.email}</p>}

            </div>
            <form onSubmit={handleSubmit}>
              <div className="text-left">
                <label className="font-bold ">Adresse e-mail</label>
                <input type='email' placeholder="Entree votre adresse email" name="email" onChange={handleDataForm} className="fortext"/>
                {error.email && <p className="form-error-msg">{error.email}</p>}
              </div>
              <input type="submit"  className="bg-[#f49f4d] rounded-[10px] w-[80px] p-[10px] m-[10px] text-white cursor-pointer custom-box-shadow " value="Envoyer" />

            </form>

          </div>
        </div>
      </div>

  </>
    )
  }