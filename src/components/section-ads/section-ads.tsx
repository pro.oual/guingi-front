"use client"
import { Carousel, Card } from 'antd';
import { FC } from 'react';
const items = [1, 2, 3, 4, 5, 6];

import './style.css'
import { Badge } from 'react-daisyui';
const files = [
    {
      title: 'Lieu de réception',
      size: '3.9 MB',
      source:
        '/assets/lieu-reception.png',
    },
    {
        title: 'IMG_4985.HEIC',
        size: '3.9 MB',
        source:
        '/assets/traiteur.png',
      },
      {
        title: 'IMG_4985.HEIC',
        size: '3.9 MB',
        source:
        '/assets/location-services.png',
      },
      {
        title: 'IMG_4985.HEIC',
        size: '3.9 MB',
        source:
        '/assets/animation.png',
      },
      {
        title: 'IMG_4985.HEIC',
        size: '3.9 MB',
        source:
        '/assets/photographe.png',
      },
      {
        title: 'IMG_4985.HEIC',
        size: '3.9 MB',
        source:
        '/assets/DJ.png',
      },
      {
        title: 'IMG_4985.HEIC',
        size: '3.9 MB',
        source:
        '/assets/videaste.png',
      },
      {
        title: 'IMG_4985.HEIC',
        size: '3.9 MB',
        source:
          'https://images.unsplash.com/photo-1582053433976-25c00369fc93?ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=512&q=80',
      },
    // More files...
  ]
  const annonces = [
    {
      id: 1,
      title: 'Titre de l\'annonce 1',
      description: 'Description courte de l\'annonce 1.',
      price: '100€',
      imageSrc: 'image1.jpg',
    },
    {
      id: 2,
      title: 'Titre de l\'annonce 2',
      description: 'Description courte de l\'annonce 2.',
      price: '150€',
      imageSrc: 'image2.jpg',
    },
    // Ajoutez d'autres annonces ici
  ];
  const { Meta } = Card;
  const SectionAds: FC<any> = () => {


  return (
    <>
            <section id="annonces" className='m-10  hidden md:block '>
              <div className='w-full flex justify-center p-15'>
                  <h3 className='text-[45px] font-bold'>Annonces</h3>
              </div>
              <div className="flex w-[80%] max-w-1475 mx-auto mt-20">
                    <div className="flex justify-center flex-col mr-30 p-10">
                      <img className="max-w-200 p-2" src="/assets/mima.png" alt="" />
                      <p className="name-profile">@Mima</p>
                      <button className="bg-black text-white">Ecrire</button>
                    </div>
                    <div className="card flex justify-between items-center">
                      <div>
                        <img className="max-w-200" src="/assets/annette-content-image.png" alt="" />
                      </div>
                      <div className="description-annonce block flex flex-col h-[80%] pl-[10px] text-[18px]">
                        <div className="status bg-[#ac3328]">Cession</div>
                        <div className="font-bold py-2">
                          Mariage - location de salle <br /><span className="price">500 €</span>
                        </div>
                        <div className='pt-[20px]'>
                          Date de publication : 15/05/2021 <br />
                          Lieu de prestation : Vitry sur Seine
                        </div>
                      </div>
                      <div className='py-10'><button className="bg-[#f59f4d] text-white ">Faire une offre</button></div>
                    </div>
                </div>
                <div className="flex w-[80%] max-w-1475 mx-auto mt-20">
                    <div className="flex justify-center flex-col mr-30 p-10">
                      <img className="max-w-200 p-2" src="/assets/mima.png" alt="" />
                      <p className="name-profile">@Mima</p>
                      <button className="bg-black text-white">Ecrire</button>
                    </div>
                    <div className="card flex justify-between items-center">
                      <div>
                        <img className="max-w-200" src="/assets/annette-content-image.png" alt="" />
                      </div>
                      <div className="description-annonce flex flex-col h-[80%] text-[18px] ">
                        <div className="status bg-[#ac3328]">Cession</div>
                        <div className="font-bold py-2">
                          Mariage - location de salle <br /><span className="price">500 €</span>
                        </div>
                        <div className='pt-[20px]'>
                          Date de publication : 15/05/2021 <br />
                          Lieu de prestation : Vitry sur Seine
                        </div>
                      </div>
                      <div className='py-10'><button className="bg-[#f59f4d]  text-white ">Faire une offre</button></div>
                    </div>
                </div>
            </section>
            <section id='annonce-mobile' className='m-10 block md:hidden'>
            <div className='text-title'>
                  <h3 className='font-bold'>Annonces</h3>
              </div>
                <div   className="container card-flex"  >
                  <div  className="card with-card">
                    <div  className="flex">
                      <img  src="/assets/flowers-little.png" alt="" />
                        <div  className="flex button-container">
                          <div >
                            <button   className="big primary" tabIndex={0}> Recherche </button>
                            </div>
                            </div>
                            <div  className="center-side column">
                              <div  className="mariage-part">
                                <p  className="bold mariage-title">Location de salle</p>
                                <p  className="bold red-color mariage-price">500 €</p>
                                </div>
                                <div  className="date-part">
                                  <div >
                                    <p  className="grey-color">15/05/2021</p>
                                    <p  className="grey-color">Vitry-sur-Seine</p>
                                    </div>
                                    <img  src="/assets/mima.png" alt="" className="max-width-mima" />
                              </div>
                              </div>
                              </div>
                              </div>
                <div  className="card with-card">
                  <div  className="flex">
                    <img  src="/assets/flowers-little.png" alt="" />
                      <div  className="flex button-container">
                        <div >
                          <button   className="big primary" tabIndex={0} ng-reflect-router-link="/guingui/list"> Recherche </button>
                          </div></div><div  className="center-side column">
                            <div  className="mariage-part">
                              <p  className="bold mariage-title">Location de salle</p>
                              <p  className="bold red-color mariage-price">500 €</p>
                              </div><div  className="date-part">
                                <div ><p  className="grey-color">15/05/2021</p>
                                <p  className="grey-color">Vitry-sur-Seine</p>
                                </div>
                                <img  src="/assets/mima.png" alt="" className="max-width-mima" />
                            </div>
                          </div>
                      </div>
                      </div>
                      </div>
            </section>
    </>
  );

}



export default SectionAds;