import React, { useState, useEffect, useRef,  } from 'react';
import { io, Socket } from 'socket.io-client';
import './style.css';
import { useAuth } from '@/src/libs/authContext';
import { format } from "date-fns";

import { useRouter, useSearchParams } from 'next/navigation';
import { Observable } from 'rxjs';
import { eventEmitter } from '@/src/libs/Event';

interface Message {
  date: any;
  content: string;
  isOwnMessage: boolean;
  avatar: any;
}
interface MessageReceiver {
  chat_id: number;
  content: string;
  file_url: string;
  is_read: boolean;
  created_at: string;
  updated_at: string;
  deleted_at: string | null;
  sender: { user_id: number; [key: string]: any };
  receiver: { user_id: number; [key: string]: any };
}
const ChatApp: React.FC = ({value}: any) => {

      const router = useRouter()
      const searchParams = useSearchParams();
      const [message, setMessage] = useState<string>('');
      const [messages, setMessages] = useState<Message[]>([]);
      const socketRef = useRef<Socket>();
      const messagesEndRef = useRef(null);

      useEffect(() => {

        const token = localStorage.getItem('authToken');
        let  userData :any = null;

        const userString = localStorage.getItem('userData');

        if (userString) {
          try {
            userData = JSON.parse(userString);
          } catch (error) {
            console.error('Error parsing userData from localStorage:', error);
          }
        }

        if (!token || !userData) {
          console.log('No authToken or userData found in localStorage');
          router.push('/auth/login'); // Rediriger vers la page de connexion ou gérer autrement
          return;
        }

        // Connect to the WebSocket server
        socketRef.current = io('ws://localhost:3001', {
          transports: ['websocket'], // Utiliser WebSocket comme transport
          auth: { token: token },
        });

        const fetchMessages = async () => {
          try {
            const response = await fetch(`http://localhost:3000/v1/chat/byuser/${searchParams.get('recipient')}`, {
              method: 'GET',
              headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${token}`,
              },
            });
        
            if (!response.ok) {
              throw new Error(`HTTP error! status: ${response.status}`);
            }
        
            const data = await response.json();
            let groupedMessages: { [key: string]: Message[] } = data.reduce((acc: any, message: any) => {
              const sender = message.sender.user_id.toString();
              if (!acc[sender] ) {
                acc[sender] = [];
              }
              acc[sender].push(message);
              return acc;
            }, {});
            console.log(groupedMessages)
            const userIdToKeep = searchParams.get('recipient');
        
            Object.keys(groupedMessages).forEach(key => {
              if (key !== userIdToKeep && key !== userData.user_id.toString()) {
                delete groupedMessages[key];
              }
            });
            const messagesArray: any[] = Object.values(groupedMessages).flat();
            const fetchedMessages = messagesArray.map(item => ({
              content: item.content,
              date:  item.created_at,
              isOwnMessage: item.sender.user_id === userData.user_id,
              avatar: item.sender === userData.user_id ? item.sender.avatar : item.receiver.avatar 

            }));
            setMessages(fetchedMessages.sort((a, b) => a.date.localeCompare(b.date)));
          } catch (error) {
            console.error('Could not fetch messages:', error);
          }
        };
        
        fetchMessages();
        


        socketRef.current.on("connect_error", (err) => {
          // the reason of the error, for example "xhr poll error"
          console.log(err.message);
        
          // some additional description, for example the status code of the initial HTTP response
        });
        // Listen for incoming messages
        socketRef.current.on('new_msg', (payload: any) => {
          console.log(payload)
          const recipientId = searchParams.get('recipient');
          const room = recipientId ? `${userData.user_id}-${recipientId}` : null;
          const room2 = recipientId ? `${recipientId}-${userData.user_id}` : null;
          if (payload.room === room) {

          } else if (payload.room === room2) {
          } else if (payload.room !== room && payload.room !== room2) {
            //eventEmitter.emit('notif', payload)
            return;
          }
          console.log(payload);
          setMessages((prevMessages) => [
            ...prevMessages,
            { 
              content: payload.message, 
              isOwnMessage: payload.sender === userData.user_id,
              date: payload.date,
              avatar: payload.sender === userData.user_id ? payload.sender.avatar : payload.receiver.avatar 
            },
          ]);
        });





        // Join the room
        (async () => {

          const recipientId = searchParams.get('recipient');
          const room = recipientId ? `${userData.user_id}-${recipientId}` : null;
          await socketRef.current?.emit('joinRoom', {
            sender: userData.user_id,
            recipient: searchParams.get('recipient'),
            room: room, 
          });
        })();

        // Cleanup on unmount
        return () => {
          socketRef.current?.disconnect();
        };
      }, [searchParams]);

      const scrollToBottom = () => {
        messagesEndRef.current?.scrollIntoView({ behavior: "smooth" });
      };

      const sendMessage = async (e: any) => {
        let  userData :any = null;

          const userString = localStorage.getItem('userData');

          if (userString) {
            try {
              userData = JSON.parse(userString);
            } catch (error) {
              console.error('Error parsing userData from localStorage:', error);
            }
          }
          if (!userData) {
            console.log('No authToken or userData found in localStorage');
            router.push('/auth/login'); // Rediriger vers la page de connexion ou gérer autrement
            return;
          }
        const recipientId = searchParams.get('recipient');
        const room = recipientId ? `${userData.user_id}-${recipientId}` : null;

        e.preventDefault();
        if (message.trim()) {
          console.log("message", message)
          await socketRef.current?.emit('message', {
            message,
            sender: userData.user_id,
            recipient: searchParams.get('recipient'),
            room: room,
          });
          setMessage('');
        }
        scrollToBottom();
      };



     const [isLoading, setIsLoading] = useState(true);
     console.log(messages);

     let previousDate = null;

     return (
      <>
        <div className="chat-list-container">
          <div className="chat-messages">
            {messages.map((msg, index) => (
              <div key={index}>
                {(() => {
                  if (msg.date) {
                    const currentDate = format(new Date(msg.date), 'dd/MM/yyyy');
                    const shouldDisplayDate = currentDate !== previousDate;
                    previousDate = currentDate;
    
                    if (shouldDisplayDate) {
                      return (
                        <div className="date-divider-container">
                          <div className="date-divider-left"></div>
                          <p className="date-text">{currentDate}</p>
                          <div className="date-divider-right"></div>
                        </div>
                      );
                    }
                  }
    
                  return null;
                })()}
                
                {msg.avatar ? (
                  <div className='flex'>
                    <div>
                      <img className='rounded mr-[2px] w-[35px] h-[35px]' src={msg.avatar} alt="" />
                    </div>
                    <div className={`rounded message ${msg.isOwnMessage ? 'message-right' : ''}`}>
                      {msg.content} 
                    </div>
                    <div>
                      {(() => {
                        if (msg.date) {
                          return (
                            <p className="text-sm p-[15px]">
                              <i>{format(new Date(msg.date), 'HH:mm')}</i>
                            </p>
                          );
                        } else {
                          const currentTime = format(new Date(), 'HH:mm'); // Obtenir l'heure actuelle si msg.date n'existe pas
                          return (
                            <p className="text-sm p-[15px]">
                              <i>{currentTime}</i>
                            </p>
                          );
                        }
                      })()}
                    </div>
                  </div>
                ) : (
                  <div className='flex justify-end'>
                    <div>
                      {(() => {
                        if (msg.date) {
                          return (
                            <p className="text-sm p-[15px]">
                              <i>{format(new Date(msg.date), 'HH:mm')}</i>
                            </p>
                          );
                        } else {
                          const currentTime = format(new Date(), 'HH:mm'); // Obtenir l'heure actuelle si msg.date n'existe pas
                          return (
                            <p className="text-sm p-[15px]">
                              <i>{currentTime}</i>
                            </p>
                          );
                        }
                      })()}
                    </div>
                    <div className={'rounded ' + `message ${msg.isOwnMessage ? 'message-right' : ''}`}>
                      {msg.content} 
                    </div>
                    <div>
                      <img className='ml-[2px] w-[35px] h-[35px] ' src="/assets/user_conn.png" alt="" />
                    </div>
                  </div>
                )}
              </div>
            ))}
            <div ref={messagesEndRef} />
          </div>
          <form className="chat-form" onSubmit={sendMessage}>
            <input
              type="text"
              value={message}
              onChange={(e) => setMessage(e.target.value)}
              placeholder="Message"
            />
            <a type='button' onClick={sendMessage}>
              <img  src="/assets/attachment.svg" alt="" className="button-chat rounded"></img>
            </a>
            <button className='p-4 bg-[#f59f4d] rounded' type="button">Faire une proposition</button>
          </form>
        </div>
      </>
    );
    
    };

export default ChatApp;
