"use client"
import { Carousel, Card } from 'antd';
import { FC } from 'react';


import './style.css'
const files = [
    {
      title: 'Lieu de réception',
      size: '3.9 MB',
      source:
        '/assets/lieu-reception.png',
    },
    {
        title: 'IMG_4985.HEIC',
        size: '3.9 MB',
        source:
        '/assets/traiteur.png',
      },
      {
        title: 'IMG_4985.HEIC',
        size: '3.9 MB',
        source:
        '/assets/location-services.png',
      },
      {
        title: 'IMG_4985.HEIC',
        size: '3.9 MB',
        source:
        '/assets/animation.png',
      },
      {
        title: 'IMG_4985.HEIC',
        size: '3.9 MB',
        source:
        '/assets/photographe.png',
      },
      {
        title: 'IMG_4985.HEIC',
        size: '3.9 MB',
        source:
        '/assets/DJ.png',
      },
      {
        title: 'IMG_4985.HEIC',
        size: '3.9 MB',
        source:
        '/assets/videaste.png',
      },
      {
        title: 'IMG_4985.HEIC',
        size: '3.9 MB',
        source:
          'https://images.unsplash.com/photo-1582053433976-25c00369fc93?ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=512&q=80',
      },
    // More files...
  ]

  const { Meta } = Card;
const SectionCategories: FC<any> = () => {
  const isFirefox = typeof navigator !== 'undefined' && navigator.userAgent.includes('Firefox');
  const isChrome = typeof navigator !== 'undefined' && navigator.userAgent.includes('Chrome');

  
  const scrollToPoint = (pointId: string) => {
    const element = document.getElementById(pointId);
    if (element) {
      if (isFirefox) {
        // Pour Firefox
        element.scrollIntoView({ behavior: 'smooth' });
      } else if (isChrome) {
        // Pour Chrome
        element.scrollIntoView();

      } else {
        // Comportement par défaut pour les autres navigateurs
        element.scrollIntoView({ behavior: 'smooth' });
      }
    }
  };
  return (
    <>
        <section className='max-[951px]:hidden' >
            <div className='flex justify-center p-10'>
              <h2 className="text-[45px] font-bold">Catégories</h2>
            </div>
            <div className='wrapper-slider'>
            <div className="grpButton">
            <button className="rounded-full bg-orange-400 h-[100px] " onClick={()=>scrollToPoint('point_1')}>
              <img
                 className="max-w-[200px]"
                src="/assets/arrow-left.svg"
                alt=""
              />
            </button>
            <button className="rounded-full bg-orange-400 h-[100px]" onClick={()=>scrollToPoint('point_8')}>
              <img
                className="max-w-[200px]"
                src="/assets/arrow-right.svg"
              />
            </button>
          </div>

          <div className="slider">
            <div className="slides">
              <div id="point_1" className="relative">
                <div className="type absolute">
                  Réception
                </div>
                <img src="/assets/lieu-reception.png" alt="" />
              </div>

              <div id="point_2" className="relative">
                <div className="type absolute">Traiteur</div>
                <img src="/assets/Traiteur.png" alt="" />
              </div>

              <div id="point_3" className="relative">
                <div className="type absolute">Services</div>
                <img src="/assets/location-services.png" alt="" />
              </div>

              <div id="point_4" className="relative">
                <div className="type absolute">Animation</div>
                <img src="/assets/animation.png" alt="" />
              </div>

              <div id="point_6" className="relative">
                <div className="type absolute left-0">Photographe</div>
                <img src="/assets/photographe.png" alt="" />
              </div>

              <div id="point_7" className="relative">
                <div className="type absolute">DJ</div>
                <img src="/assets/DJ.png" alt="" />
              </div>

              <div id="point_8" className="relative">
                <div className="type absolute">Vidéaste</div>
                <img src="/assets/Videaste.png" alt="" />
              </div>
            </div>

 



          </div>
          </div>
        </section>


    </>
  );
}

export default SectionCategories;