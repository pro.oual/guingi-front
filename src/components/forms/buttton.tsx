import { FC } from "react";
import { Button } from "react-daisyui";

const BoutonInput: FC<any> = ({value, ...props}) => {
  return (
    <Button {...props}>{value}</Button>
  );
}
export default BoutonInput;