"use client"
import { FC } from 'react';
import { Form, Radio, Tooltip } from 'react-daisyui';
const RadioInput: FC<any> = ({title,info,messageTooltip, ...props}) => {
  return (
    <>
        <Form.Label title={title}>
            <Radio {...props} />
        </Form.Label>

    </>
  );
}

export default RadioInput;