"use client"
import { FC } from 'react';
import { Button, Form, Radio } from 'react-daisyui';
const Tooltip: FC<any> = ({...props}) => {
    
  return (
    <>
      <Tooltip {...props} >
            <Button>Hover me</Button>
            <span  className='circle-red'>?</span>
      </Tooltip>
    </>
  );
}

export default Tooltip;