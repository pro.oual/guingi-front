export async function http(path: string,data: any) {
    try {
        const response = await fetch(path, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(data),
        });

        if (!response.ok) {
            throw new Error(`Erreur HTTP: ${response.status}`);
        }
        return await response.json(); // Supposons que la réponse contient des données utiles
    } catch (error) {
        console.error("Erreur lors de la connexion:", error);
        throw error; // Propager l'erreur pour une gestion ultérieure
    }
}