import React, { createContext, useState, useEffect, useCallback } from 'react';
import { eventEmitter } from './Event';

export const NotificationContext = createContext<{ 
  notifData: any;
  setNotifData: any;
 }>(
  {
    notifData: null,
    setNotifData: () => {},
  }
);

export const NotificationProvider = ({ children } : any) => {
  const [notifData, setNotifData] = useState<any>(null);
console.log("rrr")
  const handleMonEvenement = useCallback((data: any) => {
    setNotifData(data);
  }, []);

  useEffect(() => {
    eventEmitter.on('notif', handleMonEvenement);
    return () => {
      eventEmitter.off('notif', handleMonEvenement);
    };
  }, []);

  useEffect(() => {
    const inter = setTimeout(() => {
      setNotifData(null);
    }, 5000);

    return () => {
      clearInterval(inter);
    };
  }, [notifData]);

  return (
    <NotificationContext.Provider value={{ notifData, setNotifData }}>
      {children}
    </NotificationContext.Provider>
  );
};
