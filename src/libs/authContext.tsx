"use client"
import React, { createContext, useContext, useState, ReactNode, useRef } from 'react';

interface AuthContextType {
  authToken: string | null;
  login: (token: string) => void;
  logout: () => void;
  verifyToken: () => any;
}

const AuthContext = createContext<AuthContextType>(null);

export const useAuth = () => {
  const context = useContext(AuthContext);
  if (!context) {
    throw new Error("useAuth doit être utilisé à l'intérieur d'un AuthProvider");
  }
  return context;
};

interface AuthProviderProps {
  children: ReactNode;
}

export const AuthProvider: React.FC<AuthProviderProps> = ({ children }) => {
  const [authToken, setAuthToken] = useState<string | null>(null);
  const login = (token: string) => {
    localStorage.setItem('authToken', token);
    setAuthToken(token);

    return true;
  };

  const logout = () => {
    localStorage.removeItem('authToken');
    setAuthToken(null);
    location.reload();
    return true; 
  };

  const verifyToken = async () => {
    const tokenLS = localStorage.getItem('authToken');
    if(tokenLS){
    try {
        const response = await fetch(`http://localhost:3000/v1/auth/verify-token?token=${tokenLS}`, {
          method: 'GET',
        });
        if (!response.ok) {
          return false;
        }
        const user = await response.json();
        localStorage.setItem('userData', JSON.stringify(user));

        return user;
    } catch (error) {
      console.error(error);
      return false;
    }
    }
    return false;
  };

   const value = { authToken, login, logout, verifyToken };

  return (
    <AuthContext.Provider value={value}>
      {children}
    </AuthContext.Provider>
  );
};
