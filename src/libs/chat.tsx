import React, { useState, useEffect, useRef } from 'react';
import { useAuth } from '@/src/libs/authContext';

interface Message {
  content: string;
  isOwnMessage: boolean;
}
interface MessageReceiver {
  chat_id: number;
  content: string;
  file_url: string;
  is_read: boolean;
  created_at: string;
  updated_at: string;
  deleted_at: string | null;
  sender: { user_id: number; [key: string]: any };
  receiver: { user_id: number; [key: string]: any };
}

/*const useChatList = () =>{
    const [chatList, setChatList] = useState(null);
    const {userData} = useAuth();

    useEffect(() => {

        const token = localStorage.getItem('authToken')
        // Fetch previous messages
        const fetchMessages = async () => {
          try {
            const response = await fetch('http://localhost:3000/v1/chat/byuser', {
              method: 'GET',
              headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${token}`,
              },
            });
            if (!response.ok) {
              throw new Error(`HTTP error! status: ${response.status}`);
            }
            const data = await response.json();
            const groupedMessages = data.reduce((acc: any, message: any) => {
            const receiverId = message.receiver.user_id;
            
              if (!acc[receiverId]) {
                acc[receiverId] = [];
              }
            
              acc[receiverId].push(message);
              return acc;
            }, {} as { [key: number]: Message[] });

            delete groupedMessages[userData.user_id];
           
            setChatList(groupedMessages);
          } catch (error) {
            console.error('Could not fetch messages:', error);
          }
        };

        fetchMessages();

        // Listen for incoming messages
    }, [userData]);

};

export default useChatList;*/