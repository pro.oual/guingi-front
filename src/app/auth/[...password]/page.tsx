import Login from '@/src/components/base/auth/login/login'
import PasswordRecover from '@/src/components/base/auth/password-recover/password-recover'
import Image from 'next/image'

export default function PasswordRecoverPage() {

  return (

    <>
        <div className="bg-[#ac3328] page-wrap h-screen">

        <PasswordRecover />
        </div>
    </>

  )
}
