
import PasswordRecoverEmail from '@/src/components/base/auth/recover-email/recover-email'

export default function PasswordRecoverEmailPage() {

  return (

    <>
        <div className="bg-[#ac3328] page-wrap h-screen">

        <PasswordRecoverEmail />
        </div>
    </>

  )
}
