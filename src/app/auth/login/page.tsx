import { lazy } from 'react'
const LoginLazy = lazy(() => import('@/src/components/base/auth/login/login'));

export default function LoginPage() {

  return (

    <>
        <div className="bg-[#ac3328] page-wrap h-screen">

        <LoginLazy />
        </div>
    </>

  )
}
