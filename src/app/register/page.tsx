
import Register from '@/src/components/base/auth/register/register'
import Navbar from '@/src/components/base/navbar/navbar'
import { useAuth } from '@/src/libs/authContext';
import Image from 'next/image'
import { lazy, useEffect } from 'react';

const RegisterLazy = lazy(() => import('@/src/components/base/auth/register/register'));
export default function Home() {
  const liens = ['Accueil', 'À Propos', 'Services', 'Contact'];

  return (
    <>
      <Navbar links={liens} />
      <RegisterLazy value={"!auth"} />
    </>
  )
}
