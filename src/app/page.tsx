import Navbar from '../components/base/navbar/navbar'
import Footer from '../components/base/footer/footer';
import Header from '../components/base/header/header';
import SectionCategories from '../components/section-categories/section-categories';
import SectionAds from '../components/section-ads/section-ads';

export default function Home() {
  const liens = ['Accueil', 'À Propos', 'Services', 'Contact'];
  return (
    <>    
    
    <Navbar links={liens} />
    <Header />
    <SectionCategories />
    <SectionAds />
    <Footer />
    </>
  )


}