import type { Metadata } from 'next'
import { Inter } from 'next/font/google'
import './globals.css'
import { AuthProvider } from '../libs/authContext'
import localFont from 'next/font/local'


// Font files can be colocated inside of `app`
const AvenirNext = localFont({
  src: '../../public/fonts/AvenirNextCyr-Medium.woff2',
})

export const metadata: Metadata = {
  title: 'Guingi',
  description: 'une application qui permet de vendre des prestation',
}

export default function RootLayout({
  children,
}: {
  children: React.ReactNode
}) {
  return (
    <AuthProvider>

    <html lang="fr">
      <body className={AvenirNext.className}>
        {children}
        </body>

    </html>
    </AuthProvider>

  )
}
