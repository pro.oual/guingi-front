'use client'
import Navbar from '@/src/components/base/navbar/navbar';
import Image from 'next/image'
import { useState, useEffect } from 'react';

import './style.css'

export default function Services() {
  const [annonces, setAnnonces] = useState<any[]>([]);
  const [filtres, setFiltres] = useState({ region: '', type: '', prix: '', surface: '' });
  const [page, setPage] = useState(0);
  const [totalPages, setTotalPages] = useState(0);

  useEffect(() => {
    // Effectuer la récupération des données ici
  }, [filtres, page]);

  const handleChangeFiltre = (e) => {
    const { name, value } = e.target;
    setFiltres(prevFiltres => ({ ...prevFiltres, [name]: value }));
  };

  const handlePageChange = (newPage) => {
    setPage(newPage);
  };
  const liens = ['Accueil', 'À Propos', 'Services', 'Contact'];

  return (
<>    <Navbar links={liens} />

<div className="container">
      <div className="filterContainer">
        {/* Éléments de filtre ici */}
        <p>dddddddd</p>
      </div>

      <div className="annoncesContainer">
        {annonces.map(annonce => (
          <div key={annonce.id} className="annonce">
            {/* Détails de l'annonce */}
            <p>annonce</p>
          </div>
        ))}
      </div>

      <div className="paginationContainer">
        {Array.from({ length: totalPages }, (_, index) => (
          <button
            key={index}
            onClick={() => handlePageChange(index)}
            className={`button ${index === page ? 'activeButton' : ''}`}
          >
            {index + 1}
          </button>
        ))}
      </div>
    </div>
    </>
  );
}
