'use client'
import Navbar from '@/src/components/base/navbar/navbar';
import Image from 'next/image'
import { Fragment, useEffect, useState } from 'react';
import './style.css'
import { Dialog } from '@headlessui/react';
import { DialogTitle, DialogContent, DialogContentText, DialogActions } from '@mui/material';
import { format } from 'date-fns';
import { Button } from 'react-daisyui';
import { PriceChange } from '@mui/icons-material';
import Loading from './loading';
import { useRouter } from "next/navigation";

export default function Services() {
  const liens = ['Accueil', 'À Propos', 'Services', 'Contact'];
  const [localisation, setLocalisation] = useState([]);
  const [price, setPrice] = useState<any>(0);
  const [date, setDate] = useState<string>('');
  const [loading, setLoading] = useState<any>(false);
  const router = useRouter();

        const handleSubmit = async (e) => {
          e.preventDefault();
          setLoading(true)
          const tokenLS = localStorage.getItem('authToken');
          const queryParams = new URLSearchParams(window.location.search);
          //queryParams.delete(window.location.search);
          console.log(InputValue1ID, inputValue2, date, dateToTimestamp(date), radio1, price);
          try {
            if (InputValue1ID) {
                queryParams.set('type', InputValue1ID);
            }else{
              queryParams.delete('type');
            }
            if (inputValue2) {
                queryParams.set('location', inputValue2);
            }
            else{
              queryParams.delete('location');
            }
            if (date) {
              const timestampAsString = dateToTimestamp(date).toString();
                queryParams.set('date', timestampAsString);
            }
            else{
              queryParams.delete('date');
            }
            if (radio1) {
                queryParams.set('prestation', radio1);
            }
            else{
              queryParams.delete('prestation');
            }
            if (price) {
              queryParams.set('price', price);
            }
            else{
              queryParams.delete('price');
            }

           const url = `http://localhost:3000/v1/service/by-user-search?${queryParams.toString()}`; 
           history.pushState({}, '', `/services?${queryParams.toString()}`);
           const response = await fetch(url, {
              method: 'GET',
              headers: {
                'Authorization': `Bearer ${tokenLS}`,
              },
            });
            if (!response.ok) {
              return false;
            }
            const data = await response.json();
            setData(data)
            setLoading(false)
            return true;
          } catch (error) {
            console.error(error);
            setLoading(false)

            return false;
          }
        };
        useEffect(() => {

            const categories = async () => {
                try {
                    const response = await fetch(`http://localhost:3000/v1/category`, {
                    method: 'GET',
                    });
            
                    if (!response.ok) {
                    return false;
                    }
            
                    const catego = await response.json();
                    setCategories(catego)
                } catch (error) {
                console.error(error);
                return false;
                }
                return false;
            };

            categories()
        }, []);
        const [radio1, setRadio1] = useState('C');
        const [data, setData] = useState<any>([]);
        useEffect(()=>{
        const getItems = async () => {
          const searchParams = new URLSearchParams(window.location.search);
          let type, location, date, prestation;
          if (searchParams.get('type')) {
              type = searchParams.get('type')?.toString();
          }
          if (searchParams.get('location')) {
              location = searchParams.get('location')?.toString();
          }
          if (searchParams.get('date')) {
              date = searchParams.get('date')?.toString();
          }
          if (searchParams.get('prestation')) {
              prestation = searchParams.get('prestation')?.toString();
          }

          try {

            const queryParams = new URLSearchParams(); 

            if (type) {
                queryParams.append('type', type);
            }
            if (location) {
                queryParams.append('location', location);
            }
            if (date) {
                queryParams.append('date', date);
            }
            if (prestation) {
                queryParams.append('prestation', prestation);
            }
            

            const url = `http://localhost:3000/v1/service/by-user-search?${queryParams.toString()}`;
            
              const response = await fetch(url, {
                method: 'GET',
              });
      
              if (!response.ok) {
                return false;
              }
      
              const data = await response.json();

              console.log(data)


              setData(data)
              return true;
          } catch (error) {
            console.error(error);
            return false;
          }

          return false;
        };
        getItems()
        },[])
        const [inputValue, setInputValue] = useState('');
        const [InputValue1ID, setInputValue1ID] = useState<any>(0);       
        const setTypePrestationCategorie = async (e) => {
          const value = e.target.value;
          setInputValue(value);
          if (value ) {
            try {
              const likeCategoryData = { caractere: value };
              const response = await fetch(`http://localhost:3000/v1/category/search/caracteres`, {
                method: 'POST',
                body: JSON.stringify(likeCategoryData),
                headers: {
                  'Content-Type': 'application/json',
                },
              });
      
              if (!response.ok) {
                throw new Error(`Erreur HTTP: ${response.status}`);
              }
      
              const data = await response.json();
              setCategories(data);
            } catch (error) {
              console.error('Erreur lors de la requête:', error);
            }
          } else {
            setCategories([]);
          }
        };
        const [inputValue2, setInputValue2] = useState('');
        const setTypePrestationLocalisation = async (e) => {
          const value = e.target.value;
          setInputValue2(value);
      
          if (value  ) {
            try {
              const likeLocationData = { caractere: value };
              const response = await fetch(`http://localhost:3000/v1/duration-info-service/search/caracteres`, {
                method: 'POST',
                body: JSON.stringify(likeLocationData),
                headers: {
                  'Content-Type': 'application/json',
                },
              });
      
              if (!response.ok) {
                throw new Error(`Erreur HTTP: ${response.status}`);
              }
      
              const data = await response.json();
              setLocalisation(data);
            } catch (error) {
              console.error('Erreur lors de la requête:', error);
            }
          } else {
            setLocalisation([]);
          }
        };

        const handleSuggestionClick = async (suggestion) => {
            setInputValue(suggestion.name);
            setInputValue1ID(suggestion.Categorie_id);
            setCategories([]);
        };
    
        const handleSuggestionClick2 = (suggestion) => {
            // Supposons que 'suggestion' est un objet et que vous voulez utiliser une propriété 'nom'
            setInputValue2(suggestion.place_of_service);
            setLocalisation([]);
        };

        const [categories, setCategories] = useState<any>([]);

        function dateToTimestamp(dateStr: string): number {
          const date = new Date(dateStr);
          return date.getTime();
        }    
        if (loading) {
          return <Loading />; // Ou un composant de chargement personnalisé
        }

  return (
    <>    
    <Navbar links={liens} />
    <div className="App">
              <div className="grid grid-cols-4 gap-4 job-list">
          <div className='col-span-1 border ml-[29px] mt-[10px] p-[50px] h-[500px]'>

          <form  autoComplete='off' onSubmit={handleSubmit}>

                {/* Ici, vous mettriez vos filtres de recherche */}
                  <div className="p-[4px]">

                  <div className="mb-4 md:mb-0 md:mr-2">
                  <label htmlFor="beautiful-input" className="block text-gray-700 label-slide mb-2 text-[18px]">
                      Type de prestation
                  </label>
                  <input
                      id="beautiful-input"
                      type="text"
                      name="type_of_service"
                      className="rounded-lg p-3 w-full border-2 opacity-100 mt-0.5"
                      placeholder="Données"
                      value={inputValue ?? ''}
                      onChange={setTypePrestationCategorie}
                  />
                  {categories.length > 0 && (
                      <ul className="absolute z-10  bg-white w-[15%] border border-gray-300 mt-1 rounded-md shadow-lg max-h-60 overflow-auto">
                      {categories.map((suggestion, index) => (
                          <li key={index} 
                          className="p-2 hover:bg-gray-100 cursor-pointer"
                          onClick={() => handleSuggestionClick(suggestion)}>
                          {suggestion.name} {/* Assurez-vous que cela correspond à la structure de votre objet suggestion */}
                          </li>
                      ))}
                      </ul>
                  )}
                  </div>

                  <div className="mb-4 md:mb-0 md:mr-2">
                  <label htmlFor="typeService" className="block text-sm">Prix</label>
                  <select id="typeService" 
                  name="type_service"
                  className="w-full rounded-lg" 
                  required
                  value={price ?? ''}
                  onChange={(event)=>setPrice(event?.target.value)}
                  >
                  <option value={0} key={-1}>Autre ...</option>
                  <option value={100} key={100}>Max 100€</option>
                  <option value={500} key={500}>Max  500€</option>
                  <option value={1000} key={1000}>Max 1000€</option>

                  </select>
                  </div>

                  <div className="mb-4 md:mb-0 md:mr-2">
                  <label htmlFor="beautiful-input2" className="block text-gray-700 label-slide mb-2 text-[18px]">
                    Localisation
                    </label>
                    <input
                        id="beautiful-input2"
                        type="text"
                        name='localisation'
                        className="rounded-lg p-3 w-full border-2 bg-white opacity-100 mt-0.5"
                        placeholder="Données"
                        value={inputValue2 ?? ''}

                        onChange={e => setTypePrestationLocalisation(e)}
                    />
                    {localisation.length > 0 && (
                        <ul className="absolute z-10  bg-white w-[15%] border border-gray-300 mt-1 rounded-md shadow-lg max-h-60 overflow-auto">
                        {localisation.map((suggestion: any, index) => (
                            <li key={index} 
                            className="p-2 hover:bg-gray-100 cursor-pointer"
                            onClick={() => handleSuggestionClick2(suggestion)}>
                            {suggestion.place_of_service} 
                            </li>
                        ))}
                        </ul>
                    )}
                  </div>
                  <div>
                  <label htmlFor="beautiful-input" className="block text-gray-700 label-slide mb-2 text-[18px]">
                                Date
                                </label>
                                <input
                                    id="beautiful-input"
                                    type="date"
                                    name='date'
                                    className="rounded-r-lg p-3 w-full border-2 bg-white opacity-100 mt-0.5"
                                    value={date}
                                    onChange={e => setDate(e.target.value)}
                                />
                                </div>

                  </div>
                  
                  <div className='mb-4 md:mb-0 md:mr-2'>
                    <input
                    type="radio"
                    title="Cession de prestation"
                    id='r-c'
                    value="C"
                    onChange={e => setRadio1(e.target.value)}
                    className='w-8'
                    defaultChecked
                    name="radio1"
                    />
                    <label htmlFor='r-c'>Cession</label>

                    <input
                    type="radio"
                    title="Recherche de prestation"
                    id='r-r'
                    value="R"
                    onChange={e => setRadio1(e.target.value)}
                    className='w-8'
                    name="radio1"
                    />
                  <label htmlFor='r-r'>Recherche</label>

                  </div>

                  <div className='flex justify-center '>
                    <button type="submit" className='relative inline-flex items-center gap-x-1.5 rounded-md bg-[#f59f4d] px-3 py-2 text-sm font-semibold text-white shadow-sm'>Valider</button>
                  </div>


              </form>

          </div>
          <div className="col-span-3 w-full">
          {data && data.length > 0 ? (
            data.map((element, index) => (
              <div key={index} className="flex w-[85%] rounded-lg shadow-2xl max-w-1475 mx-auto mt-20 job-card">
                <div className="card flex block items-center">
                  <div>
                    {element.element.img1 ? (
                      <img className="w-[200px] h-[200px]" src={element.element.img1} alt="" />
                    ) : (
                      <div className="flex">
                        <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" strokeWidth={1.5} stroke="currentColor" className="w-6 h-6">
                          <path strokeLinecap="round" strokeLinejoin="round" d="m2.25 15.75 5.159-5.159a2.25 2.25 0 0 1 3.182 0l5.159 5.159m-1.5-1.5 1.409-1.409a2.25 2.25 0 0 1 3.182 0l2.909 2.909m-18 3.75h16.5a1.5 1.5 0 0 0 1.5-1.5V6a1.5 1.5 0 0 0-1.5-1.5H3.75A1.5 1.5 0 0 0 2.25 6v12a1.5 1.5 0 0 0 1.5 1.5Zm10.5-11.25h.008v.008h-.008V8.25Zm.375 0a.375.375 0 1 1-.75 0 .375.375 0 0 1 .75 0Z" />
                        </svg>
                        <p className="pl-[10px]">aucune photo ...</p>
                      </div>
                    )}
                  </div>
                  <div className="description-annonce block flex flex-col h-[80%] pl-[10px] text-[18px]">
                    <div className="flex flex-row">
                      <div className="status bg-[#ac3328]">
                        {element.element.type_of_service === 'R' ? 'Recherche' : 'Cessation'}
                      </div>
                      <div className="status ml-[10px]  bg-[#f59f4d]">
                        <a href={`/announcement?service=${element.element.service_id}`}>Consulter</a>
                      </div>
                    </div>
                    <div className="font-bold py-2">
                      {element.element.title} <br />
                      <span className="price"></span>
                    </div>
                    <div className="pt-[20px]">
                      Date de publication :{' '}
                      {element.element.created_at && format(new Date(element.element.created_at), 'dd/MM/yyyy HH:mm')}{' '}
                      <br />
                      {element.dis.map((dis, disIndex) => (
                        <p key={disIndex}>
                          Lieu de prestation : {dis.place_of_service},{' '}
                          {format(new Date(dis.datetime_of_start), 'dd/MM/yyyy HH:mm')}
                        </p>
                      ))}
                    </div>
                  </div>
                </div>
              </div>
            ))
          ) : (
            <p>aucun resultat</p>
          )}

          </div>
        </div>
      <div className="pagination">
        {/* Ici, vous mettriez votre composant de pagination */}
      </div>
    </div>







    
    </>

  )
}
