'use client'
import { useAuth } from "@/src/libs/authContext";
import { useRouter } from "next/navigation";
import { useEffect, useState } from "react";
import Loading from "./loading";
import Navbar from "@/src/components/base/navbar/navbar";
import { NotificationProvider } from "@/src/libs/chatContext";
import './style.css';


export default function DashboardPage() {
  const {verifyToken} = useAuth();
  const router = useRouter();
  const [isLoading, setIsLoading] = useState(true);

  
  useEffect(() => {
    const executeVerification = async () => {
      try {
        const response = await verifyToken();
        if (!response) {
          router.push("/auth/login");
        } else {
          setIsLoading(false); // Vérification réussie, afficher la page
        }
      } catch (error) {
        console.error("Erreur de vérification", error);
        router.push("/auth/login");
      }
    };
    executeVerification();
    
  }, []);

  if (isLoading) {
    return <Loading />; // Ou un composant de chargement personnalisé
  }
  const liens = ['Accueil', 'À Propos', 'Services', 'Contact'];

  return (
    <>    
        <Navbar links={liens} />
        <div className="dashboard">
        <Card
          title="Tableau de bord"
          info={"dashboard"}
          description="Pilotez et suivez vos transactions sur la plateforme Guingi."
        />
        <Card
          title="Informations personnelles"
          info={"info"}
          description="Mettez à jour les informations relatives à votre compte."
        />
        <Card
          title="Mes annonces"
          info={"announcements"}
          description="Visualisez, modifiez et supprimez vos annonces publiées."
        />
        <Card
          title="Mes favoris"
          info={"favoris"}
          description="Consultez et gérez vos annonces favorites."
        />
      </div>
    </>
  )
}

const Card = ({ title, description,info, ...props }) => {
const router = useRouter();
const handleClick = () => {
  let path = '/dashboard';
  switch (info) {
    case 'dashboard':
      path = path + '/pilot';
      break;
    case 'info':
      path =path +  '/profil';
      break;
    case 'announcements':
      path = path + '/announcements';
      break;
    case 'favoris':
      path = path +  '/wishlist';
      break;
    default:
      // Handle the default case or set a default path
  }
  router.push(path);
};
return (
  
  <div className="card cursor-pointer" onClick={handleClick}>
    {
      info == "dashboard" 
      ?
      <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" strokeWidth={1.5} stroke="currentColor" className="w-6 h-6">
      <path strokeLinecap="round" strokeLinejoin="round" d="M3 13.125C3 12.504 3.504 12 4.125 12h2.25c.621 0 1.125.504 1.125 1.125v6.75C7.5 20.496 6.996 21 6.375 21h-2.25A1.125 1.125 0 0 1 3 19.875v-6.75ZM9.75 8.625c0-.621.504-1.125 1.125-1.125h2.25c.621 0 1.125.504 1.125 1.125v11.25c0 .621-.504 1.125-1.125 1.125h-2.25a1.125 1.125 0 0 1-1.125-1.125V8.625ZM16.5 4.125c0-.621.504-1.125 1.125-1.125h2.25C20.496 3 21 3.504 21 4.125v15.75c0 .621-.504 1.125-1.125 1.125h-2.25a1.125 1.125 0 0 1-1.125-1.125V4.125Z" />
    </svg>
    :
    (info == "info") ? 
    <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" strokeWidth={1.5} stroke="currentColor" className="w-6 h-6">
      <path strokeLinecap="round" strokeLinejoin="round" d="M15.75 6a3.75 3.75 0 1 1-7.5 0 3.75 3.75 0 0 1 7.5 0ZM4.501 20.118a7.5 7.5 0 0 1 14.998 0A17.933 17.933 0 0 1 12 21.75c-2.676 0-5.216-.584-7.499-1.632Z" />
    </svg>
    : 
    (info == "announcements")  ?  
    <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" strokeWidth={1.5} stroke="currentColor" className="w-6 h-6">
       <path strokeLinecap="round" strokeLinejoin="round" d="M19.114 5.636a9 9 0 0 1 0 12.728M16.463 8.288a5.25 5.25 0 0 1 0 7.424M6.75 8.25l4.72-4.72a.75.75 0 0 1 1.28.53v15.88a.75.75 0 0 1-1.28.53l-4.72-4.72H4.51c-.88 0-1.704-.507-1.938-1.354A9.009 9.009 0 0 1 2.25 12c0-.83.112-1.633.322-2.396C2.806 8.756 3.63 8.25 4.51 8.25H6.75Z" />
    </svg>
    :
    (info == "favoris") ?
    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="currentColor" className="w-6 h-6">
  <path d="m11.645 20.91-.007-.003-.022-.012a15.247 15.247 0 0 1-.383-.218 25.18 25.18 0 0 1-4.244-3.17C4.688 15.36 2.25 12.174 2.25 8.25 2.25 5.322 4.714 3 7.688 3A5.5 5.5 0 0 1 12 5.052 5.5 5.5 0 0 1 16.313 3c2.973 0 5.437 2.322 5.437 5.25 0 3.925-2.438 7.111-4.739 9.256a25.175 25.175 0 0 1-4.244 3.17 15.247 15.247 0 0 1-.383.219l-.022.012-.007.004-.003.001a.752.752 0 0 1-.704 0l-.003-.001Z" />
</svg>
  :<></>
    }
   
    <h3>{title}</h3>
    <p>{description}</p>
  </div>
)};