'use client'

import Register from "@/src/components/base/auth/register/register";
import Navbar from "@/src/components/base/navbar/navbar";

export default function ProfilPage() {
  const liens = ['Accueil', 'À Propos', 'Services', 'Contact'];

  return (
    <>    
       <Navbar links={liens} />
       <Register value={"auth"} />
    </>
  )
}