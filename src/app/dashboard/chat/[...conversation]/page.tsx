'use client'
import Navbar from "@/src/components/base/navbar/navbar";
import Chat from "@/src/components/chat/chat";

export default function ConversationPage() {
  const liens = ['Accueil', 'À Propos', 'Services', 'Contact'];

  return (
    <>   

      <Navbar links={liens} />
      <Chat />
    </>
  )
}