"use client"
import Chat from "@/src/components/chat/chat";
import { useAuth } from "@/src/libs/authContext";
import { useRouter } from "next/navigation";
import { useEffect, useState } from "react";
import Loading from "../loading";
import { format } from 'date-fns';

import './style.css';
import Navbar from "@/src/components/base/navbar/navbar";

export default function ChatPage() {
  const router = useRouter();
  const [isLoading, setIsLoading] = useState(true);
  const [chatList, setChatList] = useState<ChatList>({});

  useEffect(() => {
    const token = localStorage.getItem('authToken');
    let user:any = null;

    const userString = localStorage.getItem('userData');
    if (userString) {
      try {
        user = JSON.parse(userString);
      } catch (error) {
        console.error('Error parsing userData from localStorage:', error);
      }
    }

    if (!token) {
      console.log('No authToken or userData found in localStorage');
      router.push('/auth/login'); // Rediriger vers la page de connexion ou gérer autrement
      return;
    }

    const fetchMessages = async () => {
      try {
        const response = await fetch('http://localhost:3000/v1/chat/byuser', {
          method: 'GET',
          headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${token}`,
          },
        });
        if (!response.ok) {
          throw new Error(`HTTP error! status: ${response.status}`);
        }

        const data = await response.json();
        const groupedMessages = data.reduce((acc: any, message: any) => {
          const receiverId = message.sender.user_id;
          if (!acc[receiverId]) {
            acc[receiverId] = [];
          }
          acc[receiverId].push(message);
          return acc;
        }, {});


        delete groupedMessages[user.user_id];
        setChatList(groupedMessages);
      
      } catch (error) {
        console.error('Could not fetch messages:', error);
      } finally {
        setIsLoading(false);
      }
    };

    fetchMessages();
  }, []);

  if (isLoading) {
    return <Loading />; // Ou un composant de chargement personnalisé
  }
  console.log(chatList);
  const liens = ['Accueil', 'À Propos', 'Services', 'Contact'];

  return (

    <>
      <Navbar links={liens} />

      <div className="chat-list-container">
        <div className="flex justify-center">
            <h2 className=" bg-[#ffd97d] p-6 rounded-lg">Messagerie</h2>
        </div>
      <ul className="chat-list">

        {Object.keys(chatList).length > 0 ? (
          Object.keys(chatList).map((key) => {
            // Prendre le dernier message de la liste pour chaque utilisateur
            const lastMessage = chatList[key][chatList[key].length - 1];
            return (
              <li key={key} className="chat-list-item">
                 <div>
                      <img className="w-[70px] h-[35px]" src="/assets/message-red.svg" alt="" />
                 </div>
                <div className="chat-list-item-content">
                  <div className="chat-list-item-name">{ key.toString() == lastMessage.receiver.user_id.toString() ? lastMessage.receiver.username : lastMessage.sender.username  } </div>
                  <div className="chat-list-item-date">
                    Dernier message: `&quot;`{lastMessage.content}`&quot;`  {format(new Date(lastMessage.created_at), 'dd/MM/yyyy HH:mm')}
                  </div>
                </div>
                <div className="chat-list-item-arrow">
                  <div className="chat-list-item-arrow">
                    <a href={`/dashboard/chat/conversation?recipient=${lastMessage.sender.user_id}`}>
                        <img className="cursor-pointer" src="/assets/right-arrow.svg" alt="arrow" />
                    </a>
                  </div>
                  
                  </div>
              </li>
            );
          })
        ) : (
          <p>Aucun message à afficher.</p>
        )}

      </ul>
    </div>
    </>
  );
}
interface Sender {
  user_id: number;
  first_name: string;
  last_name: string;
  username: string;
  phone_number: string;
  number_address: string;
  address: string;
  zip_code: string;
  city: string;
  country: string;
  genre: string;
  date_of_birth: string;
  is_active: boolean;
  email: string;
  email_verified: boolean;
  local: string | null;
  last_seen: string | null;
  is_blocked: boolean;
  block_reason: string | null;
  avatar: string;
  token_password_forget: string;
  email_verified_at: string | null;
  general_conditions: boolean;
  siret: string | null;
  website: string | null;
  facebook: string | null;
  instagram: string | null;
  pinterest: string | null;
  youtube: string | null;
  mobilite_entreprise: boolean;
  number_address_mobilite_entreprise: string | null;
  address_mobilite_entreprise: string | null;
  zip_code_mobilite_entrpeirse: string | null;
  city_mobilite_entrpeirse: string | null;
  created_at: string;
  updated_at: string;
  deleted_at: string | null;
  is_entreprise: boolean;
  role: string;
  contact_yousign: string | null;
}

interface Message {
  chat_id: number;
  content: string;
  file_url: string;
  is_read: boolean;
  created_at: string;
  updated_at: string;
  deleted_at: string | null;
  sender: Sender;
  receiver: Sender; // Supposant que la structure de 'receiver' est identique à celle de 'sender'
}

type ChatList = {
  [key: string]: Message[];
}
