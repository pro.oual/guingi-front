'use client'
import Sidebar from "@/src/components/base/sidebar/sidebar";
import { useAuth } from "@/src/libs/authContext";
import { useRouter } from "next/navigation";
import { ReactNode, Suspense, useEffect, useRef, useState } from "react";
import Loading from "../loading";
import Navbar from "@/src/components/base/navbar/navbar";
import { NotificationProvider } from "@/src/libs/chatContext";
import './style.css';
import React from "react";





interface FormElement {
    id: number;
    form: JSX.Element; // Ou tout autre type approprié pour votre formulaire
  }

  

export default function PrestationPage() {
    const {verifyToken} = useAuth();
    const router = useRouter();
    const [isLoading, setIsLoading] = useState(true);
    
    const [questions, setQuestions] = useState([{ question: '', answer: '' }]);

    const [witchForm, setWitchForm] = useState<string>('acquerir');
    const [witchFormSC, setWitchFormSC] = useState<string>('critere');
    const [witchFormSC2, setWitchFormSC2] = useState<string>('critere');



    const [data, setData] = useState<any>({});


    const [selectedOptions, setSelectedOptions] = useState<any>(false);
    const handleCheckboxChange = (event) => {
        const { value, checked, name } = event.target;
        setSelectedOptions(prev => {
            // Si la case est cochée, ajoutez ou mettez à jour sa valeur
            if (checked) {
                return { ...prev, [name]: value };
            } else {
                // Si la case est décochée, créez un nouvel objet sans cette clé
                const { [name]: _, ...rest } = prev;
                return rest;
            }
        });
    };
    function handleNext3(){
        setClassColorSC2('coordonnees');
        setWitchFormSC2('coordonnees');
    }
    function handleNext2(){
        setClassColorSC2('critere');
        setWitchFormSC2('critere');
    }
    function handleNext1(){
        setClassColorSC2('conditions');
        setWitchFormSC2('conditions');
    }
    function handlePrevious3(){
        setClassColorSC2('conditions');
        setWitchFormSC2('conditions');
    }
    function handlePrevious4(){
        setClassColorSC2('coordonnees');
        setWitchFormSC2('coordonnees');
    }
    function handleNext4(){
        setClassColorSC2('question');
        setWitchFormSC2('question');
    }
    const [image1, setImage1] = useState<string>('');
    const [image2, setImage2] = useState<string>('');
    const [image3, setImage3] = useState<string>('');
    const [image4, setImage4] = useState<string>('');
    const [balance, setBalance] = useState<string>('');
    
    const formRef = useRef<HTMLFormElement>(null);
    const [error, setError] = useState<any>({});
    const handleSubmit = async (event: any) => {
        event.preventDefault();
        const combinedData = { ...selectedOptions, ...data };
        const formData = new FormData();
        for (const key in combinedData) {
            if(combinedData[key] === "" || combinedData[key] === undefined  || combinedData[key] === null){
                alert("Veuillez completez tout les champs")
            }
          if (combinedData.hasOwnProperty(key)) {
            if (typeof combinedData[key] === 'object' && combinedData[key] !== null && combinedData[key] instanceof File) {
              formData.append(key, combinedData[key], combinedData[key].name);
            } else {
              formData.append(key, combinedData[key]);
            }
          }
        }
        const tokenLS = localStorage.getItem('authToken');
        if (tokenLS) {
            try {
                const response = await fetch(`http://localhost:3000/v1/service`, {
                    method: 'POST',
                    headers: {
                        'Authorization': `Bearer ${tokenLS}`,
                    },
                    body: formData// Assurez-vous que data est correctement formaté
                });
    
                if (!response.ok) {
                    throw new Error('Erreur de réponse du serveur');
                }
    
                router.push("/dashboard");
            } catch (error) {
                console.error(error);
                // Gérer l'affichage d'un message d'erreur
            }
        } else {
            // Gérer le cas où le token n'existe pas
        }
    };
    const [classColor, setClassColor] = useState<string>('acquerir');
    const handleType = (e: any) => {
            setWitchForm(e.currentTarget.id);
            setClassColor(e.currentTarget.id);
    };

     useEffect(() => {
            if (witchForm === 'acquerir') {
                setData(prevData => ({...prevData, "type_of_service": "Request"}));
            } else {
                setData(prevData => ({...prevData, "type_of_service": "Ceder"}));
            }
    }, [witchForm]); // Se déclenche uniquement quand witchForm change
    
    function handleFileChange(event: any) {
        const file = event.target.files?.[0];
        if (file) {
            if (file.type !== "image/jpeg" && file.type !== "image/jpg" && file.type !== "image/png" && file.type !== "application/pdf" &&  file.type !== "application/msword") {
            setError({...error, balance: "L'extension n'est pas bonne : png/jpg/pdf"});
            }
            if(file.size > 3145728 ){
            setError({...error, balance:"le fichier et trop lourd max: 3mo"});
            }
            const { name, value } = event.target;
            setData({ ...data, [name]: file });
            const reader = new FileReader();
            reader.onloadend = () => {
            switch(event.target.id){
                case 'fileInput1':
                    setImage1(reader.result as string);
                    break;
                case 'fileInput2' : 
                    setImage2(reader.result as string);
                    break;
                case 'fileInput3' :
                    setImage3(reader.result as string);
                    break;
                case 'fileInput4' : 
                    setImage4(reader.result as string);
                    break;  
                case 'fileInput4' : 
                    setBalance(reader.result as string);
                    break;  
            }
        };
            reader.readAsDataURL(file);
        }
    };

    const [classColorSC, setClassColorSC] = useState<string>('critere');
    const handleTypeSC = (e: any) => {
        if(e.currentTarget.id !== classColorSC){
            setWitchFormSC(e.currentTarget.id)
            setClassColorSC(e.currentTarget.id);
        }
    };


    const [classColorSC2, setClassColorSC2] = useState<string>('critere');
    const handleTypeSC2 = (e: any) => {
        if(e.currentTarget.id !== classColorSC2){
        setWitchFormSC2(e.currentTarget.id)
        setClassColorSC2(e.currentTarget.id);
        }
    };


    function handleDataForm(e: any){
        const { name, value } = e.target;
        setData({ ...data, [name]: value });
    }


    const [formItems, setFormItems] = useState<any>([{ id: 0 }]); // Initialiser avec un élément

    const handleAddForm = () => {
        setFormItems([...formItems, { id: formItems.length }]); // Ajouter un nouvel élément
    };

    const handleDeleteForm = (id) => {
        setFormItems(formItems.filter(item => item.id !== id)); // Supprimer un élément
    };


    const [formFAQItems, setFormFAQItems] = useState<any>([{ id: 0 }]); // Initialiser avec un élément

    const handleAddFormFAQ = () => {
        setFormFAQItems([...formFAQItems, { id: formFAQItems.length }]); // Ajouter un nouvel élément
    };

    const handleDeleteFormFAQ = (id) => {
        setFormFAQItems(formFAQItems.filter(item => item.id !== id)); // Supprimer un élément
    };

    useEffect(() => {

        const executeVerification = async () => {
        try {
            const response = await verifyToken();
            if (!response) {
            router.push("/auth/login");
            } else {
            setIsLoading(false); // Vérification réussie, afficher la page
            }
        } catch (error) {
            console.error("Erreur de vérification", error);
            router.push("/auth/login");
        }
        };
        executeVerification();
        
    }, []);

    const [categories, setCategories] = useState<any>([]);

    useEffect(() => {

        const categories = async () => {
            const tokenLS = localStorage.getItem('authToken');
            if(tokenLS){
            try {
                const response = await fetch(`http://localhost:3000/v1/category`, {
                method: 'GET',
                headers: {
                    'Authorization': `Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyX2lkIjoxLCJ1c2VybmFtZSI6ImFiZG91YSIsInJvbGUiOiJ1c2VyIiwiZW1haWwiOiJ5YW5pcy5la21hbEBnbWFpbC5jb20iLCJ1dGlsaXR5IjoiYXV0aCIsImNyZWF0ZWQiOiIyMDIzLTExLTAxVDA5OjU5OjA3LjUwN1oiLCJkZWxldGVkIjpudWxsLCJpYXQiOjE2OTk3OTg3MDksImV4cCI6MTY5OTgyNzUwOX0.oOBncpTblkKmfYQ5lT1ITmoI9g-Hf4z-6feWztkPzLg`, // Ajoutez le token d'authentification dans l'en-tête
                },
                });
        
                if (!response.ok) {
                return false;
                }
        
                const catego = await response.json();
                setCategories(catego)
            } catch (error) {
            console.error(error);
            return false;
            }
            }
            return false;
        };

        categories()
    }, []);

    const [typeCategories, setTypeCategories] = useState<any>([]);

    const [formDIS, setFormDIS] = useState<string>('')
    async function  handleSelectCategorie(id: number, e: any){
        setTypeOptionCategories([]);

            if(id == 0){
                setTypeOptionCategories([]);
                setTypeCategories([]);
                return;
            }
            const tokenLS = localStorage.getItem('authToken');
            if(tokenLS){
            try {
                const response = await fetch(`http://localhost:3000/v1/type-categorie/categorie/${id}`, {
                method: 'GET',
                headers: {
                    'Authorization': `Bearer ${tokenLS}`, // Ajoutez le token d'authentification dans l'en-tête
                },
                });
                if (!response.ok) {
                return false;
                }
                const typecatego = await response.json();
                setTypeCategories(typecatego)
                const { name, value } = e.target;
                setData({ ...data, [name]: value });
                if(id==2){
                    setFormDIS('Location')
                    return;
                }
                setFormDIS('')
            } catch (error) {
            console.error(error);
            return false;
            }
            }
            return;
    }

    const [typeOptionCategories, setTypeOptionCategories] = useState<any>([]);
    async function  handleSelectTypeCategorie(id: number, event: any){
        if(id == 0){
            setTypeOptionCategories([]);
            return;
        }
        const tokenLS = localStorage.getItem('authToken');
        if(tokenLS){
        try {
            const response = await fetch(`http://localhost:3000/v1/option-type-categorie/type-categprie/${id}`, {
                method: 'GET',
                headers: {
                'Authorization': `Bearer ${tokenLS}`, // Ajoutez le token d'authentification dans l'en-tête
            },
            });
            if (!response.ok) {
                return false;
            }
            const typeoptioncatego = await response.json();

            setTypeOptionCategories(typeoptioncatego)
            const { name, value } = event.target;
            setData({ ...data, [name]: value });
        } catch (error) {
            console.error(error);
            return false;
        }
        }
        return;
    }
    const [resultat1, setResultat1] = useState<any[]>([]);
    const [resultat2, setResultat2] = useState<any[]>([]);

    function extractFormData(form: any) {
        console.log(form)
            const formData = new FormData(form);
            const data: any = {};
            formData.forEach((value, key) => {
                data[key] = value;
            });
        return data;
    }

    function handleNext(e: any) {
        const formData = extractFormData(formRef.current);
        setResultat1([...resultat1,  formData]);
        setClassColorSC('question');
        setWitchFormSC('question');
    }

    function handlePrevious() {
        const formData = extractFormData(formRef.current);
        setResultat2([...resultat2, formData]);
        setClassColorSC('critere');
        setWitchFormSC('critere');
    }
    const [isButtonDisabled, setIsButtonDisabled] = useState(false);
    if (isLoading) {
        return <Loading />; 
    }
    const liens = ['Accueil', 'À Propos', 'Services', 'Contact'];
    console.log(data)
    return (
        <>    
        <Navbar links={liens} />
        <div className="flex flex-col items-center justify-center pt-[50px] ">

        <div className="flex flex-wrap items-center justify-center gap-4">
          {/* Carte pour Acquérir une prestation */}
          <button type="button" id="acquerir" onClick={handleType} className={`${classColor === 'acquerir' ? 'bg-[#ac352c] text-white' : 'bg-white text-black'} cursor-pointer  p-10 rounded-lg shadow-lg text-center w-1/3`}>
            <h2 className="text-xl font-bold ">Acquérir une prestation</h2>
            <div className="flex justify-center">
            <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" strokeWidth={1.5} stroke="currentColor" className="w-6 h-6 text-center">
                <path strokeLinecap="round" strokeLinejoin="round" d="M15.75 10.5V6a3.75 3.75 0 1 0-7.5 0v4.5m11.356-1.993 1.263 12c.07.665-.45 1.243-1.119 1.243H4.25a1.125 1.125 0 0 1-1.12-1.243l1.264-12A1.125 1.125 0 0 1 5.513 7.5h12.974c.576 0 1.059.435 1.119 1.007ZM8.625 10.5a.375.375 0 1 1-.75 0 .375.375 0 0 1 .75 0Zm7.5 0a.375.375 0 1 1-.75 0 .375.375 0 0 1 .75 0Z" />
            </svg>
            </div>
            <p className="text-center">Exemple : j`&quot;`acquiers une prestation de traiteur auprès d`&quot;`un particulier ou d`&apos;`un professionnel.</p>
        </button>

        <button type="button" id="ceder" onClick={handleType} className={`${classColor === 'ceder' ? 'bg-[#ac352c] text-white' : 'bg-white text-black'} cursor-pointer rounded-lg shadow-lg text-center w-1/3 p-10`}>
            <h2 className="text-xl font-bold">Céder une prestation</h2>
            <div className="flex justify-center">
            <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" strokeWidth={1.5} stroke="currentColor" className="w-6 h-6">
                <path strokeLinecap="round" strokeLinejoin="round" d="M3 16.5v2.25A2.25 2.25 0 0 0 5.25 21h13.5A2.25 2.25 0 0 0 21 18.75V16.5m-13.5-9L12 3m0 0 4.5 4.5M12 3v13.5" />
            </svg>
            </div>
            <p>Exemple : je cède ma prestation de photographe car je l`&quot;`ai annulé.</p>
        </button>
        </div>
            {
                witchForm == 'acquerir' ? 
                <div className="flex mt-8">
                <button id="critere" onClick={handleTypeSC} className={`${classColorSC == "critere" ? 'bg-[#ffd97d] text-white' : 'bg-white text-black'} px-6 py-2 rounded-lg shadow-md mr-2`}>
                    
                    Critères de la prestation</button>
                <button id="question" onClick={handleTypeSC} className={`${classColorSC == "question" ? 'bg-[#ffd97d] text-white' : 'bg-white text-black'}  px-6 py-2 rounded-lg shadow-md ml-2`}>
                    Questions/réponses</button>
                </div>
                :
                <div className="flex mt-8 gap-4">
                <button id="critere" onClick={handleTypeSC2} 
                className={`${classColorSC2 == 'critere' ? 'bg-[#ffd97d] text-white' : 'bg-white text-black'} px-6 py-2 rounded-lg shadow-md mr-2`}>
                Critères de la prestation
                </button>

                <button id="conditions" onClick={handleTypeSC2} 
                className={`${classColorSC2 == 'conditions' ? 'bg-[#ffd97d] text-white' : 'bg-white text-black'}  px-6 py-2 rounded-lg shadow-md ml-2`}>
                Conditions financières
                </button>

                <button id="coordonnees" onClick={handleTypeSC2} 
                className={`${classColorSC2 == 'coordonnees' ? 'bg-[#ffd97d] text-white' : 'bg-white text-black'} px-6 py-2 rounded-lg shadow-md mr-2`}>
                Coordonnées du prestataire</button>

                <button id="question" onClick={handleTypeSC2} 
                className={`${classColorSC2 == 'question' ? 'bg-[#ffd97d] text-white' : 'bg-white text-black'}  px-6 py-2 rounded-lg shadow-md ml-2`}>
                Questions/réponses</button>
                </div>
            }
        </div>
        <div className="min-h-screen p-5">
        <div className="mx-auto bg-white p-6 rounded-md shadow-md">
        <form ref={formRef} onSubmit={handleSubmit}  encType="multipart/form-data">

            {
                witchForm == 'acquerir' ? 
                witchFormSC == 'critere' ? 
                (
                <div>
                <div className="mb-4">
                    <h1 className="text-xl font-bold text-gray-700">Critères de la prestation</h1>
                </div>
                <div className="grid grid-cols-1 md:grid-cols-2 gap-6">
                    <div>
                    <input type="hidden" name="type_of_service" value={"Request"} />
                    <label htmlFor="title" className="block text-sm font-medium text-gray-700 asterix--after">Titre de l`&quot;`annonce</label>
                    <input
                        type="text"
                        id="title"
                        className="mt-1 block w-full rounded-md  fortext "
                        placeholder="Ecrivez un titre"
                        name="title"
                        onChange={handleDataForm}
                        value={data.title ?? ''}
                        required
                    />
                    <label htmlFor="description" className="block text-sm font-medium text-gray-700 mt-4 asterix--after">Description</label>
                    <textarea
                        id="description"
                        name="description"
                        rows={4}
                        className="mt-1 block w-full rounded-md bg-[#F9F8F8] "
                        placeholder="Ajoutez une description"
                        onChange={handleDataForm}
                        value={data.description ?? ''}
                        required


                    />
                    </div>
                    {/* Colonne de droite */}
                    <div className="pl-[30%]">
                    <div className="flex flex-wrap m-2">
                        <div className="p-2">
                        <div className="bg-white p-4 rounded-lg shadow-lg text-center">
                            <div className="mb-2">
                            <img src={image1} alt="" className='w-[100px]'  />
                            {
                                !image1 ? 
                                <span className="inline-block p-3 rounded-full bg-red-100">
                                    <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" strokeWidth={1.5} stroke="currentColor" className="w-6 h-6">
                                        <path strokeLinecap="round" strokeLinejoin="round" d="M6.827 6.175A2.31 2.31 0 0 1 5.186 7.23c-.38.054-.757.112-1.134.175C2.999 7.58 2.25 8.507 2.25 9.574V18a2.25 2.25 0 0 0 2.25 2.25h15A2.25 2.25 0 0 0 21.75 18V9.574c0-1.067-.75-1.994-1.802-2.169a47.865 47.865 0 0 0-1.134-.175 2.31 2.31 0 0 1-1.64-1.055l-.822-1.316a2.192 2.192 0 0 0-1.736-1.039 48.774 48.774 0 0 0-5.232 0 2.192 2.192 0 0 0-1.736 1.039l-.821 1.316Z" />
                                        <path strokeLinecap="round" strokeLinejoin="round" d="M16.5 12.75a4.5 4.5 0 1 1-9 0 4.5 4.5 0 0 1 9 0ZM18.75 10.5h.008v.008h-.008V10.5Z" />
                                    </svg>
                                </span>
                            :
                            <>
                            </>
                            }
      
                            </div>
                            <input type="file" name="img1" id="fileInput1" style={{ display: 'none' }} onChange={handleFileChange} />
                            <label htmlFor="fileInput1" className="text-sm text-gray-600 cursor-pointer">
                                Ajouter une photo
                            </label>
                        </div>

                        </div>
                        <div className="p-2">
                        <div className="bg-white p-4 rounded-lg shadow-lg text-center">
                            <div className="mb-2">


                            <img src={image2} alt="" className='w-[100px]' />
                            {
                                !image2 ? 
                                <span className="inline-block p-3 rounded-full bg-red-100">
                                    <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" strokeWidth={1.5} stroke="currentColor" className="w-6 h-6">
                                        <path strokeLinecap="round" strokeLinejoin="round" d="M6.827 6.175A2.31 2.31 0 0 1 5.186 7.23c-.38.054-.757.112-1.134.175C2.999 7.58 2.25 8.507 2.25 9.574V18a2.25 2.25 0 0 0 2.25 2.25h15A2.25 2.25 0 0 0 21.75 18V9.574c0-1.067-.75-1.994-1.802-2.169a47.865 47.865 0 0 0-1.134-.175 2.31 2.31 0 0 1-1.64-1.055l-.822-1.316a2.192 2.192 0 0 0-1.736-1.039 48.774 48.774 0 0 0-5.232 0 2.192 2.192 0 0 0-1.736 1.039l-.821 1.316Z" />
                                        <path strokeLinecap="round" strokeLinejoin="round" d="M16.5 12.75a4.5 4.5 0 1 1-9 0 4.5 4.5 0 0 1 9 0ZM18.75 10.5h.008v.008h-.008V10.5Z" />
                                    </svg>
                                </span>
                            :
                            <>
                            </>
                            }
                            </div>
                            <input type="file" name="img2" id="fileInput2" style={{ display: 'none' }} onChange={handleFileChange} />
                            <label htmlFor="fileInput2" className="text-sm text-gray-600 cursor-pointer">
                                Ajouter une photo
                            </label>
                        </div>
                        </div>
                        <div className="p-2">
                        <div className="bg-white p-4 rounded-lg shadow-lg text-center">
                            <div className="mb-2">
                            <img src={image3} alt="" className='w-[100px]' />
                            {
                                !image3 ? 
                                <span className="inline-block p-3 rounded-full bg-red-100">
                                    <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" strokeWidth={1.5} stroke="currentColor" className="w-6 h-6">
                                        <path strokeLinecap="round" strokeLinejoin="round" d="M6.827 6.175A2.31 2.31 0 0 1 5.186 7.23c-.38.054-.757.112-1.134.175C2.999 7.58 2.25 8.507 2.25 9.574V18a2.25 2.25 0 0 0 2.25 2.25h15A2.25 2.25 0 0 0 21.75 18V9.574c0-1.067-.75-1.994-1.802-2.169a47.865 47.865 0 0 0-1.134-.175 2.31 2.31 0 0 1-1.64-1.055l-.822-1.316a2.192 2.192 0 0 0-1.736-1.039 48.774 48.774 0 0 0-5.232 0 2.192 2.192 0 0 0-1.736 1.039l-.821 1.316Z" />
                                        <path strokeLinecap="round" strokeLinejoin="round" d="M16.5 12.75a4.5 4.5 0 1 1-9 0 4.5 4.5 0 0 1 9 0ZM18.75 10.5h.008v.008h-.008V10.5Z" />
                                    </svg>
                                </span>
                            :
                            <>
                            </>
                            }


                            </div>
                            <input type="file" name="img3" id="fileInput3" style={{ display: 'none' }} onChange={handleFileChange} />
                            <label htmlFor="fileInput3" className="text-sm text-gray-600 cursor-pointer">
                                Ajouter une photo
                            </label>
                        </div>
                        </div>
                        <div className="p-2">
                        <div className="bg-white p-4 rounded-lg shadow-lg text-center">
                            <div className="mb-2">
                            <img src={image4} alt="" className='w-[100px]' />
                            {
                                !image4 ? 
                                <span className="inline-block p-3 rounded-full bg-red-100">
                                    <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" strokeWidth={1.5} stroke="currentColor" className="w-6 h-6">
                                        <path strokeLinecap="round" strokeLinejoin="round" d="M6.827 6.175A2.31 2.31 0 0 1 5.186 7.23c-.38.054-.757.112-1.134.175C2.999 7.58 2.25 8.507 2.25 9.574V18a2.25 2.25 0 0 0 2.25 2.25h15A2.25 2.25 0 0 0 21.75 18V9.574c0-1.067-.75-1.994-1.802-2.169a47.865 47.865 0 0 0-1.134-.175 2.31 2.31 0 0 1-1.64-1.055l-.822-1.316a2.192 2.192 0 0 0-1.736-1.039 48.774 48.774 0 0 0-5.232 0 2.192 2.192 0 0 0-1.736 1.039l-.821 1.316Z" />
                                        <path strokeLinecap="round" strokeLinejoin="round" d="M16.5 12.75a4.5 4.5 0 1 1-9 0 4.5 4.5 0 0 1 9 0ZM18.75 10.5h.008v.008h-.008V10.5Z" />
                                    </svg>
                                </span>
                            :
                            <>
                            </>
                            }
                            </div>
                            <input type="file" name="img4" id="fileInput4" style={{ display: 'none' }} onChange={handleFileChange} />
                            <label htmlFor="fileInput4" className="text-sm text-gray-600 cursor-pointer">
                                Ajouter une photo
                            </label>
                        </div>
                        </div>
                        {/* Répétez pour les emplacements de photo 2, 3, 4... */}
                    </div>
                    </div>


                </div>

                <div className="">
                        <div className="mx-auto  rounded-md">
                        {/* Ligne pour le type de prestation et le nom */}
                        <div className="flex flex-col md:flex-row justify-between items-center mb-4">
                            <div className="flex-1 mb-4 md:mb-0 md:mr-2">
                            <label htmlFor="typeService" className="block text-sm font-medium asterix--after">Type de prestation</label>
                            <select id="typeService" 
                            name="type_service"
                            defaultValue={data.type_service ?? ''}
                            className="w-full fortext" 
                            onChange={(event)=>handleSelectCategorie(parseInt(event.target.value, 10), event)}
                            required
                            >
                            <option value={0} key={-1}>Autre ...</option>
                            {categories && categories.map((element: any, index: any) => (
                                <option key={index} 
                                value={element.Categorie_id}>
                                    {element.name}
                                </option>
                            ))}
                            </select>
                            </div>
                            <div className="flex-1 md:ml-2">
                            
                            {
                            typeCategories.length > 0 ? 
                            <>
                            <label htmlFor="typeCategorie" className="block text-sm font-medium asterix--after">Nom de la prestation</label>
                            <select 
                            id="typeCategorie" 
                            name="type_categorie" 
                            onChange={(event)=>handleSelectTypeCategorie(parseInt(event.target.value, 10),event)}
                            defaultValue={data.type_categorie ?? ''}
                            className="w-full fortext"
                            required>
                            <option value={0} key={-1}>Autre...</option>
                            {typeCategories && typeCategories.map((element: any, index: any) => (
                            <option key={index} value={element.TypeCategorie_id} 
                            >
                                {element.name}
                            </option>
                            ))}
                            </select>
                            </>
                            :
                            <>
                            <label htmlFor="nomPrestation" className="block text-sm font-medium text-gray-700 asterix--after">Nom de la prestation</label>
                            <input type="text"  id="nomPrestation" 
                              onChange={handleDataForm}
                              value={data.type_categorie ?? ''}
                              required
                              name="type_categorie"
                              className="w-full mt-1 p-2 fortext" 
                              placeholder="Nom de la prestation" />
                            </>
                            }
                            </div>
                        </div>
                        {
                            typeOptionCategories.length > 0 ? 
                            <div className="flex flex-col md:flex-row justify-between items-center mb-4">
                                <div className="flex-1 mb-4 md:mb-0 md:mr-2">
                                    <p>La prestation comprend-elle des options?</p>
                                    {
                                        typeOptionCategories.map((element: any, index: any) => (
                                            <div key={index}>
                                                <input
                                                    type="checkbox"
                                                    id={`checkbox-${index}`}
                                                    name={`optionsType[${index}][OptionTypeCategorie_id]`}
                                                    value={element.OptionTypeCategorie_id ?? ''}
                                                    onChange={handleCheckboxChange}
                                                />
                                                <label htmlFor={`checkbox-${index}`}>{element.name}</label>
                                            </div>
                                        ))    
                                    }
                                </div>
                            </div>
                            :
                            null
                        }
                        <div>
                        {
                            typeCategories.length > 0 ? 
                            <div className="flex flex-col md:flex-row justify-between items-center mb-4">
                                <div className="flex-1 mb-4 md:mb-0 md:mr-2">
                                    {
                                        
                                            formDIS === "Location" ?
                                            <React.Fragment key={"LOC"}> 
                                                <div className="bg-red w-[10px] h-[20px]">
                                                    {"DEBUT"}
                                                </div>
                                                <div className="flex flex-col md:flex-row justify-between items-center mb-4">
                                                    <div className="flex-1 mb-4 md:mb-0 md:mr-2">
                                                        <label htmlFor={`date-location`} className="block text-sm font-medium text-gray-700 asterix--after">Date</label>
                                                        <input type="datetime-local" 
                                                        value={data["dis[0][datetime_of_start]"] ?? '' } 
                                                        id={`date-location`} onChange={handleDataForm} 
                                                        name="dis[0][datetime_of_start]" className="w-full fortext " required />
                                                    </div>

                                                    <div className="flex-1 w-full md:ml-2">
                                                        <label htmlFor={`lieu-location`} className="block text-sm font-medium text-gray-700 asterix--after">Lieu de la prestation</label>
                                                        <input type="text" id={`lieu-location`} 
                                                        value={data["dis[0][place_of_start]"] ?? ''} 
                                                        required
                                                        onChange={handleDataForm} name="dis[0][place_of_start]" className="w-full fortext" placeholder="Paris, Lyon, Nantes..." />
                                                    </div>
                                                </div>
                                                <div className="bg-red w-[10%]">{"FIN"}</div>

                                                <div className="flex flex-col md:flex-row justify-between items-center mb-4">
                                                    <div className="flex-1 mb-4 md:mb-0 md:mr-2">
                                                        <label htmlFor={`date`} className="block text-sm font-medium text-gray-700 asterix--after">Date</label>
                                                        <input type="datetime-local" id={`date`} 
                                                        value={data["dis[0][datetime_of_end]"] ?? '' } 
                                                        onChange={handleDataForm} name="dis[0][datetime_of_end]" className="w-full fortext"  required/>
                                                    </div>

                                                    <div className="flex-1 w-full md:ml-2">
                                                        <label htmlFor={`lieuPrestation`} className="block text-sm font-medium text-gray-700 asterix--after">Lieu de la prestation</label>
                                                        <input type="text" id={`lieuPrestation`} onChange={handleDataForm} 
                                                        value={data["dis[0][place_of_end]"] ?? ''} 
                                                        name="dis[0][place_of_end]" 
                                                        required
                                                        className="w-full fortext" placeholder="Paris, Lyon, Nantes..." />
                                                    </div>
                                                </div>
                                            </React.Fragment>
                                            :
                                            (
                                            <React.Fragment key={"AUTRE"}>
                                             {formItems.map((item, index) => (
                                                <div key={item.id} id={"addFBIL" + item.id} className="flex flex-col md:flex-row justify-between items-center mb-4">
                                                   <div className="flex-1 mb-4 md:mb-0 md:mr-2">
                                                    <label htmlFor="datePrestation" className="block text-sm font-medium text-gray-700 asterix--after">Date</label>
                                                    <input required type="datetime-local" value={data[`dis[${item.id}][datetime_of_start]`] ?? ''}           onChange={handleDataForm} name={`dis[${item.id}][datetime_of_start]`} id="datePrestation" className="w-full fortext" />
                                                    </div>
                                                    <div className="flex-1 mb-4 md:mb-0">
                                                    <label htmlFor="nombreInvites" className="block text-sm font-medium text-gray-700 asterix--after">Nombre d`&apos;`invités</label>
                                                    <input required type="number" id="nombreInvites" value={data[`dis[${item.id}][number_of_guest]`] ?? ''} onChange={handleDataForm} name={`dis[${item.id}][number_of_guest]`} className="w-full fortext" placeholder="0" />
                                                    </div>
                                                    <div className="flex-1 w-full md:ml-2">
                                                    <label htmlFor="lieuPrestation" className="block text-sm font-medium text-gray-700 asterix--after">Lieu de la prestation</label>
                                                    <input required type="text" id="lieuPrestation" value={data[`dis[${item.id}][place_of_service]`] ?? ''} onChange={handleDataForm} name={`dis[${item.id}][place_of_service]`} className="w-full fortext" placeholder="Paris, Lyon, Nantes..." />
                                                    </div>
                                                    <button
                                                    className="flex ml-[10px] items-center px-6 py-4 border border-transparent text-base font-medium rounded-md text-[#ac352c] bg-white shadow-sm hover:bg-gray-100 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-[#ac352c]"
                                                    onClick={() => handleDeleteForm(item.id)}>
                                                    Supprimer
                                                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="currentColor" className="ml-2 -mr-1 h-5 w-5">
                                                        <path fillRule="evenodd" d="M4.25 12a.75.75 0 0 1 .75-.75h14a.75.75 0 0 1 0 1.5H5a.75.75 0 0 1-.75-.75Z" clipRule="evenodd" />
                                                    </svg>
                                                    </button>
                                                
                                                </div>
                                            ))}

                                            <button
                                                type="button"
                                                className="flex mt-[10px] items-center px-6 py-4 border border-transparent text-base font-medium rounded-md text-[#ac352c] bg-white shadow-sm hover:bg-gray-100 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-[#ac352c]"
                                                onClick={handleAddForm}
                                            >
                                            Ajouter
                                            <svg className="ml-2 -mr-1 h-5 w-5" fill="currentColor" viewBox="0 0 20 20">
                                                <path fillRule="evenodd" d="M10 5a1 1 0 01 1 1v3h3a1 1 0 110 2h-3v3a1 1 0 01-2 0v-3H5a1 1 0 110-2h3V6a1 1 0 011-1z" clipRule="evenodd" />
                                            </svg>
                                            </button>
                                            </React.Fragment>
                                            )
                                    }
                                </div>
                            </div>
                            :
                            null
                        }


                        <div className="flex justify-end mb-4">

                            <button disabled={isButtonDisabled} type="button" id="StepNext2" onClick={(event)=>handleNext(event)} className= {isButtonDisabled ?  "bg-grey text-white px-6 py-2 rounded-lg shadow-md" : "bg-[#ac352c] text-white px-6 py-2 rounded-lg shadow-md"}>
                            Étape suivante</button>

                        </div>
                        <div className="bg-orange-100 border-l-4 border-orange-500 text-orange-700 p-4">
                        <p className="font-bold">Informations</p>
                        <p className="text-sm">
                        Votre prestation étant déjà passé, votre gain peut être affecté pour les raisons suivantes :
                        </p>
                        <ul className="text-sm text-gray-600 list-disc pl-5  text-orange-700">
                            <li>Évolution de la grille tarifaire du prestataire</li>
                            <li>Évolution des conditions générales de ventes du prestataire</li>
                            <li>Changement de date de la prestation</li>
                        </ul>
                    </div>
                    </div>
                </div>
                </div>
                </div>
                )
                : witchFormSC == 'question' ? 
                (
                <div className="py-8">

                <div className="mb-8">
                <h2 className="text-xl font-bold text-gray-700">Questions/réponses</h2>
                <p className="text-gray-600">Gagnez du temps en créant une FAQ dans votre annonce !</p>
                <div className="bg-orange-100 border-l-4 border-orange-500 text-orange-700 p-4">
                        <p className="font-bold">Informations</p>
                        <p className="text-sm">
                        Vous n`&apos;`avez pas d`&apos;`idée ? Pas de panique ! Vous aurez toujours la possibilité de revenir sur cette étape ultérieurement.
                        </p>
                </div>
                </div>


                {formFAQItems.map((item, index) => (
                <div key={"QA" + item.id} id={"QAID" + item.id} className="grid grid-cols-1 md:grid-cols-2 gap-4 mb-4">
                    <div>
                    <label htmlFor={`question-${item.id}`} className="block text-gray-700 asterix--after">
                        Question
                    </label>
                    <input
                        type="text"
                        name={`qa[question][${item.id}]`}
                        id={`question-${item.id}`}
                        className="mt-1 block w-full fortext"
                        value={data[`qa[question][${item.id}]`] ?? ''}
                        onChange={handleDataForm}
                        required
                    />
                    </div>
                    <div>

                    <label htmlFor={`answer-${item.id}`} className="block text-gray-700 asterix--after">
                        Réponse
                    </label>
                    <textarea
                        name={`qa[answer][${item.id}]`}
                        id={`answer-${item.id}`}
                        className="mt-1 block fortext"
                        rows={3}
                        value={data[`qa[answer][${item.id}]`] ?? ''}
                        onChange={handleDataForm}
                        required
                    ></textarea>
                    </div>
                    {
                        item.id == 0 ?
                        <></>
                        :
                        <div>
                        <button
                        type="button"
                        className="flex ml-[10px] items-center px-6 py-4 border border-transparent text-base font-medium rounded-md text-[#ac352c] bg-white shadow-sm hover:bg-gray-100 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-[#ac352c]"
                        onClick={() => handleDeleteFormFAQ(item.id)}>
                        Supprimer
                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="currentColor" className="ml-2 -mr-1 h-5 w-5">
                            <path fillRule="evenodd" d="M4.25 12a.75.75 0 0 1 .75-.75h14a.75.75 0 0 1 0 1.5H5a.75.75 0 0 1-.75-.75Z" clipRule="evenodd" />
                        </svg>
                        </button>
                        </div>
                    }
                   
                </div>
                ))}

                <button
                type="button"
                className="flex items-center px-6 py-2 mb-[10px] border border-transparent text-base font-medium rounded-md text-[#ac352c] bg-white shadow-sm "
                onClick={handleAddFormFAQ}>
                Ajouter une question
                <svg className="ml-2 -mr-1 h-5 w-5" fill="currentColor" viewBox="0 0 20 20">
                <path fillRule="evenodd" d="M10 5a1 1 0 01 1 1v3h3a1 1 0 110 2h-3v3a1 1 0 01-2 0v-3H5a1 1 0 110-2h3V6a1 1 0 011-1z" clipRule="evenodd" />
                </svg>
                </button>
                    
                <div className="flex justify-between">
                <button
                    className="px-6 py-2 border border-transparent text-base font-medium rounded-md text-white bg-[#ac352c] shadow-sm"
                    onClick={handlePrevious}>
                    étape precedente
                </button>
                <button
                    className="px-6 py-2 border border-transparent text-base font-medium rounded-md text-white bg-[#ac352c] shadow-sm"
                >
                    Enregistrer et quitter
                </button>
                </div>
                </div>
                ) 
                : 
                (

                <div className="py-8">
                <div className="mb-8">
                <h2 className="text-xl font-bold text-gray-700">Questions/réponses</h2>
                <p className="text-gray-600">Gagnez du temps en créant une FAQ dans votre annonce !</p>
                <div className="bg-orange-100 border-l-4 border-orange-500 text-orange-700 p-4">
                        <p className="font-bold">Informations</p>
                        <p className="text-sm">
                        Vous n`&apos;`avez pas d`&apos;`idée ? Pas de panique ! Vous aurez toujours la possibilité de revenir sur cette étape ultérieurement.
                        </p>
                    </div>
                    </div>
                        {questions.map((item, index) => (
                        <div key={index} className="grid grid-cols-1 md:grid-cols-2 gap-4 mb-4">
                            <div>
                            <label htmlFor={`question-${index}`} className="block text-gray-700">
                                Question n°{index + 1} (400 caractères maximum)
                            </label>
                            <input
                                type="text"
                                name="question"
                                id={`question-${index}`}
                                className="mt-1 block w-full px-3 py-2 bg-white border border-gray-300 rounded-md shadow-sm placeholder-gray-400 focus:outline-none focus:ring-indigo-500 focus:border-indigo-500"
                            />
                            </div>
                            <div>

                            <label htmlFor={`answer-${index}`} className="block text-gray-700">
                                Réponse n°{index + 1} (400 caractères maximum)
                            </label>
                            <textarea
                                name="answer"
                                id={`answer-${index}`}
                                className="mt-1 block w-full px-3 py-2 bg-white border border-gray-300 rounded-md shadow-sm placeholder-gray-400 focus:outline-none focus:ring-indigo-500 focus:border-indigo-500"
                                rows={3}
                            ></textarea>
                            </div>
                        </div>
                        ))}
                    <div className="flex justify-between items-center">
                <button
                    className="px-6 py-2 border border-transparent text-base font-medium rounded-md text-white bg-indigo-600 shadow-sm hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500"
                >
                    Enregistrer et quitter
                </button>
                <button
                    className="flex items-center px-6 py-2 border border-transparent text-base font-medium rounded-md text-indigo-600 bg-white shadow-sm hover:bg-gray-100 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500"
                >
                    Ajouter une question
                    <svg className="ml-2 -mr-1 h-5 w-5" fill="currentColor" viewBox="0 0 20 20">
                    <path fillRule="evenodd" d="M10 5a1 1 0 01 1 1v3h3a1 1 0 110 2h-3v3a1 1 0 01-2 0v-3H5a1 1 0 110-2h3V6a1 1 0 011-1z" clipRule="evenodd" />
                    </svg>
                </button>
                </div>
                </div> 
                )
                : 
                witchFormSC2 == 'critere' ? 
                (
                    <div>
                    <div className="mb-4">
                        <h1 className="text-xl font-bold text-gray-700">Critères de la prestation</h1>
                    </div>
                    <div className="grid grid-cols-1 md:grid-cols-2 gap-6">
                        <div>
                        <input type="hidden" name="type_of_service" value={"Request"} />
                        <label htmlFor="title" className="block text-sm font-medium text-gray-700 asterix--after">Titre de l`&quot;`annonce</label>
                        <input
                            type="text"
                            id="title"
                            className="mt-1 block w-full rounded-md  fortext "
                            placeholder="Ecrivez un titre"
                            name="title"
                            onChange={handleDataForm}
                            value={data.title ?? ''}
                            required
                        />
                        <label htmlFor="description" className="block text-sm font-medium text-gray-700 mt-4 asterix--after">Description</label>
                        <textarea
                            id="description"
                            name="description"
                            rows={4}
                            className="mt-1 block w-full rounded-md bg-[#F9F8F8] "
                            placeholder="Ajoutez une description"
                            onChange={handleDataForm}
                            value={data.description ?? ''}
                            required
    
    
                        />
                        </div>
                        {/* Colonne de droite */}
                        <div className="pl-[30%]">
                        <div className="flex flex-wrap m-2">
                            <div className="p-2">
                            <div className="bg-white p-4 rounded-lg shadow-lg text-center">
                                <div className="mb-2">
                                <img src={image1} alt="" className='w-[100px]'  />
                                {
                                    !image1 ? 
                                    <span className="inline-block p-3 rounded-full bg-red-100">
                                        <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" strokeWidth={1.5} stroke="currentColor" className="w-6 h-6">
                                            <path strokeLinecap="round" strokeLinejoin="round" d="M6.827 6.175A2.31 2.31 0 0 1 5.186 7.23c-.38.054-.757.112-1.134.175C2.999 7.58 2.25 8.507 2.25 9.574V18a2.25 2.25 0 0 0 2.25 2.25h15A2.25 2.25 0 0 0 21.75 18V9.574c0-1.067-.75-1.994-1.802-2.169a47.865 47.865 0 0 0-1.134-.175 2.31 2.31 0 0 1-1.64-1.055l-.822-1.316a2.192 2.192 0 0 0-1.736-1.039 48.774 48.774 0 0 0-5.232 0 2.192 2.192 0 0 0-1.736 1.039l-.821 1.316Z" />
                                            <path strokeLinecap="round" strokeLinejoin="round" d="M16.5 12.75a4.5 4.5 0 1 1-9 0 4.5 4.5 0 0 1 9 0ZM18.75 10.5h.008v.008h-.008V10.5Z" />
                                        </svg>
                                    </span>
                                :
                                <>
                                </>
                                }
          
                                </div>
                                <input type="file" name="img1" id="fileInput1" style={{ display: 'none' }} onChange={handleFileChange} />
                                <label htmlFor="fileInput1" className="text-sm text-gray-600 cursor-pointer">
                                    Ajouter une photo
                                </label>
                            </div>
    
                            </div>
                            <div className="p-2">
                            <div className="bg-white p-4 rounded-lg shadow-lg text-center">
                                <div className="mb-2">
    
    
                                <img src={image2} alt="" className='w-[100px]' />
                                {
                                    !image2 ? 
                                    <span className="inline-block p-3 rounded-full bg-red-100">
                                        <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" strokeWidth={1.5} stroke="currentColor" className="w-6 h-6">
                                            <path strokeLinecap="round" strokeLinejoin="round" d="M6.827 6.175A2.31 2.31 0 0 1 5.186 7.23c-.38.054-.757.112-1.134.175C2.999 7.58 2.25 8.507 2.25 9.574V18a2.25 2.25 0 0 0 2.25 2.25h15A2.25 2.25 0 0 0 21.75 18V9.574c0-1.067-.75-1.994-1.802-2.169a47.865 47.865 0 0 0-1.134-.175 2.31 2.31 0 0 1-1.64-1.055l-.822-1.316a2.192 2.192 0 0 0-1.736-1.039 48.774 48.774 0 0 0-5.232 0 2.192 2.192 0 0 0-1.736 1.039l-.821 1.316Z" />
                                            <path strokeLinecap="round" strokeLinejoin="round" d="M16.5 12.75a4.5 4.5 0 1 1-9 0 4.5 4.5 0 0 1 9 0ZM18.75 10.5h.008v.008h-.008V10.5Z" />
                                        </svg>
                                    </span>
                                :
                                <>
                                </>
                                }
                                </div>
                                <input type="file" name="img2" id="fileInput2" style={{ display: 'none' }} onChange={handleFileChange} />
                                <label htmlFor="fileInput2" className="text-sm text-gray-600 cursor-pointer">
                                    Ajouter une photo
                                </label>
                            </div>
                            </div>
                            <div className="p-2">
                            <div className="bg-white p-4 rounded-lg shadow-lg text-center">
                                <div className="mb-2">
                                <img src={image3} alt="" className='w-[100px]' />
                                {
                                    !image3 ? 
                                    <span className="inline-block p-3 rounded-full bg-red-100">
                                        <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" strokeWidth={1.5} stroke="currentColor" className="w-6 h-6">
                                            <path strokeLinecap="round" strokeLinejoin="round" d="M6.827 6.175A2.31 2.31 0 0 1 5.186 7.23c-.38.054-.757.112-1.134.175C2.999 7.58 2.25 8.507 2.25 9.574V18a2.25 2.25 0 0 0 2.25 2.25h15A2.25 2.25 0 0 0 21.75 18V9.574c0-1.067-.75-1.994-1.802-2.169a47.865 47.865 0 0 0-1.134-.175 2.31 2.31 0 0 1-1.64-1.055l-.822-1.316a2.192 2.192 0 0 0-1.736-1.039 48.774 48.774 0 0 0-5.232 0 2.192 2.192 0 0 0-1.736 1.039l-.821 1.316Z" />
                                            <path strokeLinecap="round" strokeLinejoin="round" d="M16.5 12.75a4.5 4.5 0 1 1-9 0 4.5 4.5 0 0 1 9 0ZM18.75 10.5h.008v.008h-.008V10.5Z" />
                                        </svg>
                                    </span>
                                :
                                <>
                                </>
                                }
    
    
                                </div>
                                <input type="file" name="img3" id="fileInput3" style={{ display: 'none' }} onChange={handleFileChange} />
                                <label htmlFor="fileInput3" className="text-sm text-gray-600 cursor-pointer">
                                    Ajouter une photo
                                </label>
                            </div>
                            </div>
                            <div className="p-2">
                            <div className="bg-white p-4 rounded-lg shadow-lg text-center">
                                <div className="mb-2">
                                <img src={image4} alt="" className='w-[100px]' />
                                {
                                    !image4 ? 
                                    <span className="inline-block p-3 rounded-full bg-red-100">
                                        <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" strokeWidth={1.5} stroke="currentColor" className="w-6 h-6">
                                            <path strokeLinecap="round" strokeLinejoin="round" d="M6.827 6.175A2.31 2.31 0 0 1 5.186 7.23c-.38.054-.757.112-1.134.175C2.999 7.58 2.25 8.507 2.25 9.574V18a2.25 2.25 0 0 0 2.25 2.25h15A2.25 2.25 0 0 0 21.75 18V9.574c0-1.067-.75-1.994-1.802-2.169a47.865 47.865 0 0 0-1.134-.175 2.31 2.31 0 0 1-1.64-1.055l-.822-1.316a2.192 2.192 0 0 0-1.736-1.039 48.774 48.774 0 0 0-5.232 0 2.192 2.192 0 0 0-1.736 1.039l-.821 1.316Z" />
                                            <path strokeLinecap="round" strokeLinejoin="round" d="M16.5 12.75a4.5 4.5 0 1 1-9 0 4.5 4.5 0 0 1 9 0ZM18.75 10.5h.008v.008h-.008V10.5Z" />
                                        </svg>
                                    </span>
                                :
                                <>
                                </>
                                }
                                </div>
                                <input type="file" name="img4" id="fileInput4" style={{ display: 'none' }} onChange={handleFileChange} />
                                <label htmlFor="fileInput4" className="text-sm text-gray-600 cursor-pointer">
                                    Ajouter une photo
                                </label>
                            </div>
                            </div>
                            {/* Répétez pour les emplacements de photo 2, 3, 4... */}
                        </div>
                        </div>
    
    
                    </div>
    
                    <div className="">
                            <div className="mx-auto  rounded-md">
                            {/* Ligne pour le type de prestation et le nom */}
                            <div className="flex flex-col md:flex-row justify-between items-center mb-4">
                                <div className="flex-1 mb-4 md:mb-0 md:mr-2">
                                <label htmlFor="typeService" className="block text-sm font-medium asterix--after">Type de prestation</label>
                                <select id="typeService" 
                                name="type_service"
                                defaultValue={data.type_service ?? ''}
                                className="w-full fortext" 
                                onChange={(event)=>handleSelectCategorie(parseInt(event.target.value, 10), event)}
                                required
                                >
                                <option value={0} key={-1}>Autre ...</option>
                                {categories && categories.map((element: any, index: any) => (
                                    <option key={index} 
                                    value={element.Categorie_id}>
                                        {element.name}
                                    </option>
                                ))}
                                </select>
                                </div>
                                <div className="flex-1 md:ml-2">
                                
                                {
                                typeCategories.length > 0 ? 
                                <>
                                <label htmlFor="typeCategorie" className="block text-sm font-medium asterix--after">Nom de la prestation</label>
                                <select 
                                id="typeCategorie" 
                                name="type_categorie" 
                                onChange={(event)=>handleSelectTypeCategorie(parseInt(event.target.value, 10),event)}
                                defaultValue={data.type_categorie ?? ''}
                                className="w-full fortext"
                                required>
                                <option value={0} key={-1}>Autre...</option>
                                {typeCategories && typeCategories.map((element: any, index: any) => (
                                <option key={index} value={element.TypeCategorie_id} 
                                >
                                    {element.name}
                                </option>
                                ))}
                                </select>
                                </>
                                :
                                <>
                                <label htmlFor="nomPrestation" className="block text-sm font-medium text-gray-700 asterix--after">Nom de la prestation</label>
                                <input type="text"  id="nomPrestation" 
                                  onChange={handleDataForm}
                                  value={data.type_categorie ?? ''}
                                  required
                                 
                                
                                name="type_categorie" className="w-full mt-1 p-2 fortext" placeholder="Nom de la prestation" />
                                </>
                                }
                                </div>
                            </div>
                            {
                                typeOptionCategories.length > 0 ? 
                                <div className="flex flex-col md:flex-row justify-between items-center mb-4">
                                    <div className="flex-1 mb-4 md:mb-0 md:mr-2">
                                        <p>La prestation comprend-elle des options?</p>
                                        {
                                            typeOptionCategories.map((element: any, index: any) => (
                                                <div key={index}>
                                                    <input
                                                        type="checkbox"
                                                        id={`checkbox-${index}`}
                                                        name={`optionsType[${index}][OptionTypeCategorie_id]`}
                                                        value={element.OptionTypeCategorie_id ?? ''}
                                                        onChange={handleCheckboxChange}
                                                    />
                                                    <label htmlFor={`checkbox-${index}`}>{element.name}</label>
                                                </div>
                                            ))    
                                        }
                                    </div>
                                </div>
                                :
                                null
                            }
                            <div>
                            {
                                typeCategories.length > 0 ? 
                                <div className="flex flex-col md:flex-row justify-between items-center mb-4">
                                    <div className="flex-1 mb-4 md:mb-0 md:mr-2">
                                        {
                                            
                                                formDIS === "Location" ?
                                                <React.Fragment key={"LOC"}> 
                                                    <div className="bg-red w-[10px] h-[20px]">
                                                        {"DEBUT"}
                                                    </div>
                                                    <div className="flex flex-col md:flex-row justify-between items-center mb-4">
                                                        <div className="flex-1 mb-4 md:mb-0 md:mr-2">
                                                            <label htmlFor={`date-location`} className="block text-sm font-medium text-gray-700 asterix--after">Date</label>
                                                            <input type="datetime-local" 
                                                            value={data["dis[0][datetime_of_start]"] ?? '' } 
                                                            id={`date-location`} onChange={handleDataForm} 
                                                            name="dis[0][datetime_of_start]" className="w-full fortext " required />
                                                        </div>
    
                                                        <div className="flex-1 w-full md:ml-2">
                                                            <label htmlFor={`lieu-location`} className="block text-sm font-medium text-gray-700 asterix--after">Lieu de la prestation</label>
                                                            <input type="text" id={`lieu-location`} 
                                                            value={data["dis[0][place_of_start]"] ?? ''} 
                                                            required
                                                            onChange={handleDataForm} name="dis[0][place_of_start]" className="w-full fortext" placeholder="Paris, Lyon, Nantes..." />
                                                        </div>
                                                    </div>
                                                    <div className="bg-red w-[10%]">{"FIN"}</div>
    
                                                    <div className="flex flex-col md:flex-row justify-between items-center mb-4">
                                                        <div className="flex-1 mb-4 md:mb-0 md:mr-2">
                                                            <label htmlFor={`date`} className="block text-sm font-medium text-gray-700 asterix--after">Date</label>
                                                            <input type="datetime-local" id={`date`} 
                                                            value={data["dis[0][datetime_of_end]"] ?? '' } 
                                                            onChange={handleDataForm} name="dis[0][datetime_of_end]" className="w-full fortext"  required/>
                                                        </div>
    
                                                        <div className="flex-1 w-full md:ml-2">
                                                            <label htmlFor={`lieuPrestation`} className="block text-sm font-medium text-gray-700 asterix--after">Lieu de la prestation</label>
                                                            <input type="text" id={`lieuPrestation`} onChange={handleDataForm} 
                                                            value={data["dis[0][place_of_end]"] ?? ''} 
                                                            name="dis[0][place_of_end]" 
                                                            required
                                                            className="w-full fortext" placeholder="Paris, Lyon, Nantes..." />
                                                        </div>
                                                    </div>
                                                </React.Fragment>
                                                :
                                                (
                                                <React.Fragment key={"AUTRE"}>
                                                 {formItems.map((item, index) => (
                                                    <div key={item.id} id={"addFBIL" + item.id} className="flex flex-col md:flex-row justify-between items-center mb-4">
                                                       <div className="flex-1 mb-4 md:mb-0 md:mr-2">
                                                        <label htmlFor="datePrestation" className="block text-sm font-medium text-gray-700 asterix--after">Date</label>
                                                        <input required type="datetime-local" value={data[`dis[${item.id}][datetime_of_start]`] ?? ''}           onChange={handleDataForm} name={`dis[${item.id}][datetime_of_start]`} id="datePrestation" className="w-full fortext" />
                                                        </div>
                                                        <div className="flex-1 mb-4 md:mb-0">
                                                        <label htmlFor="nombreInvites" className="block text-sm font-medium text-gray-700 asterix--after">Nombre d`&apos;`invités</label>
                                                        <input required type="number" id="nombreInvites" value={data[`dis[${item.id}][number_of_guest]`] ?? ''} onChange={handleDataForm} name={`dis[${item.id}][number_of_guest]`} className="w-full fortext" placeholder="0" />
                                                        </div>
                                                        <div className="flex-1 w-full md:ml-2">
                                                        <label htmlFor="lieuPrestation" className="block text-sm font-medium text-gray-700 asterix--after">Lieu de la prestation</label>
                                                        <input required type="text" id="lieuPrestation" value={data[`dis[${item.id}][place_of_service]`] ?? ''} onChange={handleDataForm} name={`dis[${item.id}][place_of_service]`} className="w-full fortext" placeholder="Paris, Lyon, Nantes..." />
                                                        </div>
                                                        <button
                                                        className="flex ml-[10px] items-center px-6 py-4 border border-transparent text-base font-medium rounded-md text-[#ac352c] bg-white shadow-sm hover:bg-gray-100 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-[#ac352c]"
                                                        onClick={() => handleDeleteForm(item.id)}>
                                                        Supprimer
                                                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="currentColor" className="ml-2 -mr-1 h-5 w-5">
                                                            <path fillRule="evenodd" d="M4.25 12a.75.75 0 0 1 .75-.75h14a.75.75 0 0 1 0 1.5H5a.75.75 0 0 1-.75-.75Z" clipRule="evenodd" />
                                                        </svg>
                                                        </button>
                                                    
                                                    </div>
                                                ))}
    
                                                <button
                                                    type="button"
                                                    className="flex mt-[10px] items-center px-6 py-4 border border-transparent text-base font-medium rounded-md text-[#ac352c] bg-white shadow-sm hover:bg-gray-100 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-[#ac352c]"
                                                    onClick={handleAddForm}
                                                >
                                                Ajouter
                                                <svg className="ml-2 -mr-1 h-5 w-5" fill="currentColor" viewBox="0 0 20 20">
                                                    <path fillRule="evenodd" d="M10 5a1 1 0 01 1 1v3h3a1 1 0 110 2h-3v3a1 1 0 01-2 0v-3H5a1 1 0 110-2h3V6a1 1 0 011-1z" clipRule="evenodd" />
                                                </svg>
                                                </button>
                                                </React.Fragment>
                                                )
                                        }
                                    </div>
                                </div>
                                :
                                null
                            }
    
    
                            <div className="flex justify-end mb-4">
                                <button disabled={isButtonDisabled} 
                                type="button" id="StepNext2" 
                                onClick={(event)=>handleNext1()} 
                                className= {isButtonDisabled ?  "bg-grey text-white px-6 py-2 rounded-lg shadow-md" : "bg-[#ac352c] text-white px-6 py-2 rounded-lg shadow-md"}>
                                Étape suivante</button>
                            </div>
                            <div className="bg-orange-100 border-l-4 border-orange-500 text-orange-700 p-4">
                            <p className="font-bold">Informations</p>
                            <p className="text-sm">
                            Votre prestation étant déjà passé, votre gain peut être affecté pour les raisons suivantes :
                            </p>
                            <ul className="text-sm text-gray-600 list-disc pl-5  text-orange-700">
                                <li>Évolution de la grille tarifaire du prestataire</li>
                                <li>Évolution des conditions générales de ventes du prestataire</li>
                                <li>Changement de date de la prestation</li>
                            </ul>
                        </div>
                        </div>
                    </div>
                    </div>
                    </div>
                )
                : 
                witchFormSC2 == 'conditions' ? 
                (
                  
                  <div className=" mx-auto p-4 bg-white rounded-md">
                    <div className="mb-4">
                        <h1 className="text-xl font-bold text-gray-700">Conditions financières</h1>
                    </div>
                    <div className="w-full  m-auto">
                        <div className="flex flex-wrap -mx-2">
                        <div className="w-full md:w-1/4 px-2 mb-6">
                            <label className="block text-gray-700 text-sm mb-2 asterix--after" htmlFor="totalAmount">
                            Montant total de la prestation
                            </label>
                            <input
                            className="appearance-none block w-full bg-gray-200 text-gray-700 border rounded py-2 px-4 leading-tight fortext"
                            id="totalAmount"
                            name="total_amount_service"
                            type="number"
                            placeholder="0"
                            //value={ data.total_amount_service ?? ''}
                            onChange={(e) => {
                                // Utiliser une expression régulière pour vérifier si la valeur est numérique
                                if (/^\d*$/.test(e.target.value)) {

                                    handleDataForm(e);
                                }
                            }}
                            onKeyDown={(e) => {
                                // Autoriser seulement les chiffres, les touches de navigation, et Backspace/Delete
                                if (!["0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "Backspace", "Delete", "ArrowLeft", "ArrowRight", "Tab"].includes(e.key)) {
                                    e.preventDefault();
                                }
                            }}
                            required
                            />
                        </div>
                        <div className="w-full md:w-1/4 px-2 mb-6">
                            <label className="block text-gray-700 text-sm asterix--after mb-2" htmlFor="alreadyPaid">
                            Montant déjà réglé
                            </label>
                            <input
                            className="appearance-none block w-full bg-gray-200 text-gray-700 border rounded py-2 px-4 leading-tight fortext"
                            id="alreadyPaid"
                            type="number"
                            name="already_paid"
                            placeholder="0"
                           // value={ data.already_paid ?? ''}
                            onChange={(e) => {
                                // Utiliser une expression régulière pour vérifier si la valeur est numérique
                                if (/^\d*$/.test(e.target.value)) {
                                    handleDataForm(e);
                                }
                            }}
                            required
                            onKeyDown={(e) => {
                                // Autoriser seulement les chiffres, les touches de navigation, et Backspace/Delete
                                if (!["0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "Backspace", "Delete", "ArrowLeft", "ArrowRight", "Tab"].includes(e.key)) {
                                    e.preventDefault();
                                }
                            }}
                            />
                        </div>
                        <div className="w-full md:w-1/4 px-2 mb-6">
                            <label className="block text-gray-700 text-sm asterix--after mb-2" htmlFor="amountWantSell">
                            Montant auquel je souhaite vendre
                            </label>
                            <input
                            className="appearance-none block w-full bg-gray-200 text-gray-700 border rounded py-2 px-4 leading-tight fortext"
                            id="amountWantSell"
                            type="number"
                            name="amount_want_sell"
                            placeholder="0"
                            //value={ data.amount_want_sell ?? ''}
                            onChange={(e) => {
                                const enteredValue = parseInt(e.target.value, 10);
                                const minAmount = data.total_amount_service * 0.3;
                                const maxAmount = data.total_amount_service - minAmount;
                                console.log(enteredValue)
                                if (enteredValue < minAmount || enteredValue > maxAmount || isNaN(enteredValue) ) {
                                    setError({
                                        ...error, 
                                        amount_want_sell: `Le montant doit être compris entre ${minAmount}€ et ${maxAmount}€`
                                    });
                                    setData({
                                        ...data, new_price: 0
                                    })
                                } 
                                else {
                                    setError({
                                        ...error,
                                        amount_want_sell: ''
                                    });

                                    setData(prevData => {
                                        const newData = {
                                            ...prevData,
                                            new_price: enteredValue + (enteredValue * 0.15),
                                            amount_want_sell: enteredValue
                                        };
                                        console.log('Mise à jour des données :', newData);
                                        return newData;
                                    });

                                    console.log(data.new_price)

                                   // handleDataForm(e);
                                }
                            }}
                            required
                            onKeyDown={(e) => {
                                // Allow only numeric and specific control keys
                                if (!["0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "Backspace", "Delete", "ArrowLeft", "ArrowRight", "Tab"].includes(e.key)) {
                                    e.preventDefault();
                                }
                            }}
                        />
                        {error.amount_want_sell && <p className="text-[red]">{error.amount_want_sell}</p>}   

                        </div>
                        <div className="w-full md:w-1/4 px-2 mb-6">
                            <label className="block cursor-no-drop text-gray-700 text-sm mb-2" htmlFor="newPrice">
                            Nouveau prix de la prestation TTC
                            </label>
                            <input
                            className="appearance-none cursor-no-drop block w-full bg-gray-200 text-gray-700 border rounded py-2 px-4 leading-tight fortext"
                            id="newPrice"
                            type="text"
                            value={data.new_price ?? ''}
                            name="new_price"
                            placeholder="0"
                            disabled
                            />
                        </div>
                        {/* Repeat for other monetary input fields */}
                        </div>
                        <div className="flex flex-wrap -mx-2 mb-6">
                        <div className="w-full px-2 mb-6">
                            <div className="p-4 bg-orange-100 border-l-4 border-orange-500 text-orange-700">
                            <p className="font-bold">Informations</p>
                            <ul>
                                <li className="text-sm">Le prix peut évoluer en fonction des conditions générales de vente du prestataire (saisonnalité, jours fériés etc...).</li>
                                <li className="text-sm">Voici le prix qui apparaîtra sur votre annonce après déduction des frais de gestion de 15 %.</li>
                            </ul>
                            </div>
                        </div>
                        </div>
                        {/* Radio buttons section */}
                        <div className="flex flex-wrap -mx-2 mb-6">
                        <div className="w-full md:w-1/2 px-2 mb-6 md:mb-0">
                            <label className="block text-gray-700 text-sm font-bold mb-2 asterix--after">
                            Votre prestataire a-t-il déjà accepté que vous cédiez votre prestation à une autre personne ?
                            </label>
                            <div className="mt-2">
                            <label className="inline-flex items-center">
                                <input type="radio" className="form-radio" name="provider_accepted" value={"0"} onChange={handleDataForm} required />
                                <span className="ml-2">Oui</span>
                            </label>
                            <label className="inline-flex items-center ml-6">
                                <input type="radio" className="form-radio" name="provider_accepted" value={"1"} onChange={handleDataForm} required />
                                <span className="ml-2">Non</span>
                            </label>
                            <label className="inline-flex items-center ml-6">
                                <input type="radio" className="form-radio" name="provider_accepted" value="2" onChange={handleDataForm} required />
                                <span className="ml-2">Je ne sais pas/Je n`&apos;`ai pas demandé</span>
                            </label>
                            </div>
                        </div>
                        <div className="w-full md:w-1/2 px-2 mb-6 md:mb-0">
                            <label className="block text-gray-700 text-sm font-bold mb-2 asterix--after">
                            Avez-vous un avoir ?
                            </label>
                            <div className="mt-2">
                            <label className="inline-flex items-center">
                                <input type="radio" className="form-radio" name="have_balance" onChange={handleDataForm} required value={"0"} />
                                <span className="ml-2">Oui</span>
                            </label>
                            <label className="inline-flex items-center ml-6">
                                <input type="radio" className="form-radio" name="have_balance" onChange={handleDataForm} value={"1"} required  />
                                <span className="ml-2">Non</span>
                            </label>
                        </div>
                        {/* Repeat for other questions */}
                        </div>
                        </div>
                        <div className="flex flex-wrap -mx-2 mt-4">
                        <div className="w-full md:w-1/4 px-2 mb-6">
                            <label className="block text-gray-700 text-sm asterix--after mb-2" htmlFor="date_validity_balance">
                            Date de validité de l`&apos;`avoir 
                            </label>
                            <input type="datetime-local" 
                            value={data.date_validity_balance ?? '' } 
                            id={`date_validity_balance`} onChange={handleDataForm} 
                            name="date_validity_balance" className="w-full fortext" required />
                        </div>
                        <div className="mt-[30px]">
                        <label htmlFor="urlBalance" className="block custom-file-input-button ">
                        <span className="md:mx-[100px]">Charger le document</span>
                        <input type="file" id="urlBalance" name="url_balance" 
                        className="bg-black text-white sm:w-full" 
                        onChange={handleFileChange}
                        />
                        </label>
                        </div>
                            {data.url_balance && <p className="mt-[40px]">{data.url_balance.name} </p>}
                            {error.balance && <p className="mt-[40px] text-[red]">{error.balance} </p>}
                        </div>
                        <div className="flex flex-wrap -mx-2 mt-4 justify-center md:justify-end">
                            <button type="button" onClick={handleNext2} className="mx-2 mb-2 bg-[#ac352c]  text-white  py-2 px-4 rounded">
                            Étape precedente
                            </button>
                            <button type="button" onClick={handleNext3} className="mx-2 mb-2 bg-[#ac352c]  text-white  py-2 px-4 rounded">
                            Étape suivante
                            </button>
                        </div>
                    </div>
                    </div>
                )
                : witchFormSC2 == 'coordonnees' ? 
                (
                <>
                    <div className="bg-white rounded px-8 pt-6 pb-8 mb-4">
                    <div className="mb-4">
                        <h1 className="text-xl font-bold text-gray-700">Coordonnées du prestataire</h1>
                    </div>
                    <p className="mb-6 text-sm text-gray-700">
                      Ces informations nous serviront pour faire une proposition à votre prestataire. Un prestataire est plus susceptible d`&apos;`accepter votre cession si vous avez déjà une proposition de reprise.
                    </p>
                      <div className="mb-4 md:flex md:items-end">
                        <div className="md:w-1/3 px-3 mb-6 md:mb-0">
                          <label className="block tracking-wide text-gray-700 text-sm asterix--after mb-2" htmlFor="provider-name">
                            Nom du prestataire
                          </label>
                          <input
                            className="appearance-none block w-full fortext"
                            id="provider-name"
                            type="text"
                            name="provider_name"
                            value={data.provider_name ?? ''}
                            onChange={(e) => handleDataForm(e)}
                            placeholder="Saisissez le prestataire"
                            required
                          />
                        </div>
                        <div className="md:w-1/3 px-3 mb-6 md:mb-0">
                          <label className="block  tracking-wide text-gray-700 text-sm asterix--after  mb-2" htmlFor="email">
                            E-mail
                          </label>
                          <input
                            className="appearance-none block w-full fortext"
                            id="email"
                            type="email"
                            name="email_provider"
                            value={data.email_provider ?? ''}
                            onChange={(e) => handleDataForm(e)}
                            placeholder="Saisissez un mail"
                            required
                          />
                        </div>
                        <div className="md:w-1/3 px-3">
                          <label className="block  tracking-wide text-gray-700 text-sm asterix--after mb-2" htmlFor="phone">
                            Téléphone
                          </label>
                          <input
                            className="appearance-none block w-full fortext"
                            id="phone"
                            type="tel"
                            name="phone_number"
                            value={data.phone_number ?? ''}
                            onChange={(e) => handleDataForm(e)}
                            placeholder="Saisissez le téléphone"
                            required
                          />
                        </div>
                      </div>
                      {/* Repeat structure for website, Instagram, and Facebook fields */}
              
                      <div className="flex items-center justify-end mt-8 gap-4">
                      <button
                          className="bg-[#ac352c]   text-white py-2 px-4 rounded"
                          type="button" // This should be changed to "submit" or handle the next step logic
                          onClick={handlePrevious3}
                        >
                          Étape precedente
                        </button>
                        <button
                          className="bg-[#ac352c]   text-white  py-2 px-4 rounded"
                          type="button" // This should be changed to "submit" or handle the next step logic
                          onClick={handleNext4}

                        >
                          Étape suivante
                        </button>
                      </div>
                  </div>
                </>
                )
                : witchFormSC2 == 'question' ? 
                (
                    <div className="py-8">

                    <div className="mb-8">
                    <h2 className="text-xl font-bold text-gray-700">Questions/réponses</h2>
                    <p className="text-gray-600">Gagnez du temps en créant une FAQ dans votre annonce !</p>
                    <div className="bg-orange-100 border-l-4 border-orange-500 text-orange-700 p-4">
                            <p className="font-bold">Informations</p>
                            <p className="text-sm">
                            Vous n`&apos;`avez pas d`&apos;`idée ? Pas de panique ! Vous aurez toujours la possibilité de revenir sur cette étape ultérieurement.
                            </p>
                    </div>
                    </div>
    
    
                    {formFAQItems.map((item, index) => (
                    <div key={"QA" + item.id} id={"QAID" + item.id} className="grid grid-cols-1 md:grid-cols-2 gap-4 mb-4">
                        <div>
                        <label htmlFor={`question-${item.id}`} className="block text-gray-700 asterix--after">
                            Question
                        </label>
                        <input
                            type="text"
                            name={`qa[question][${item.id}]`}
                            id={`question-${item.id}`}
                            className="mt-1 block w-full fortext"
                            value={data[`qa[question][${item.id}]`] ?? ''}
                            onChange={handleDataForm}
                            required
                        />
                        </div>
                        <div>
    
                        <label htmlFor={`answer-${item.id}`} className="block text-gray-700 asterix--after">
                            Réponse
                        </label>
                        <textarea
                            name={`qa[answer][${item.id}]`}
                            id={`answer-${item.id}`}
                            className="mt-1 block fortext"
                            rows={3}
                            value={data[`qa[answer][${item.id}]`] ?? ''}
                            onChange={handleDataForm}
                            required
                        ></textarea>
                        </div>
                        {
                            item.id == 0 ?
                            <></>
                            :
                            <div>
                            <button
                            type="button"
                            className="flex ml-[10px] items-center px-6 py-4 border border-transparent text-base font-medium rounded-md text-[#ac352c] bg-white shadow-sm hover:bg-gray-100 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-[#ac352c]"
                            onClick={() => handleDeleteFormFAQ(item.id)}>
                            Supprimer
                            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="currentColor" className="ml-2 -mr-1 h-5 w-5">
                                <path fillRule="evenodd" d="M4.25 12a.75.75 0 0 1 .75-.75h14a.75.75 0 0 1 0 1.5H5a.75.75 0 0 1-.75-.75Z" clipRule="evenodd" />
                            </svg>
                            </button>
                            </div>
                        }
                       
                    </div>
                    ))}
    
                    <button
                    type="button"
                    className="flex items-center px-6 py-2 mb-[10px] border border-transparent text-base font-medium rounded-md text-[#ac352c] bg-white shadow-sm "
                    onClick={handleAddFormFAQ}>
                    Ajouter une question
                    <svg className="ml-2 -mr-1 h-5 w-5" fill="currentColor" viewBox="0 0 20 20">
                    <path fillRule="evenodd" d="M10 5a1 1 0 01 1 1v3h3a1 1 0 110 2h-3v3a1 1 0 01-2 0v-3H5a1 1 0 110-2h3V6a1 1 0 011-1z" clipRule="evenodd" />
                    </svg>
                    </button>
                        
                    <div className="flex justify-between">
                    <button
                        className="px-6 py-2 border border-transparent text-base font-medium rounded-md text-white bg-[#ac352c] shadow-sm"
                        onClick={handlePrevious4}>
                        étape precedente
                    </button>
                    <button
                        className="px-6 py-2 border border-transparent text-base font-medium rounded-md text-white bg-[#ac352c] shadow-sm"
                    >
                        Enregistrer et quitter
                    </button>
                    </div>
                    </div>
                ) 
                :
                <></>
            }
        </form>
        </div>
    </div>
    </>
  )
}