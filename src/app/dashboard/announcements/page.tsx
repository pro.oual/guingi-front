'use client'
import { useAuth } from "@/src/libs/authContext";
import { useRouter } from "next/navigation";
import { MouseEvent, useCallback, useEffect, useRef, useState } from "react";
import Loading from "../loading";
import Navbar from "@/src/components/base/navbar/navbar";
import './style.css';
import { format } from "date-fns";
import { Fragment } from 'react'

import Button from '@mui/material/Button';
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogContentText from '@mui/material/DialogContentText';
import DialogTitle from '@mui/material/DialogTitle';



export default function AnnouncementPage() {
  const {verifyToken} = useAuth();
  const router = useRouter();
  const [isLoading, setIsLoading] = useState(true);
  const [showDialog, setShowDialog] = useState(false);
  const [open, setOpen] = useState(false);

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  useEffect(() => {
    const executeVerification = async () => {
      try {
        const response = await verifyToken();
        if (!response) {
          router.push("/auth/login");
        } else {
          setIsLoading(false); // Vérification réussie, afficher la page
        }
      } catch (error) {
        console.error("Erreur de vérification", error);
        router.push("/auth/login");
      }
    };
    executeVerification();
    
  }, []);


  const [data, setData] = useState<any>([]);

  useEffect(() => {
    const getItems = async () => {
        const tokenLS = localStorage.getItem('authToken');
        if(tokenLS){
        try {
            const response = await fetch(`http://localhost:3000/v1/service/by-user`, {
              method: 'GET',
              headers: {
                'Authorization': `Bearer ${tokenLS}`,
              },
            });
    
            if (!response.ok) {
              return false;
            }
    
            const data = await response.json();
            setData(data)
            return true;
        } catch (error) {
          console.error(error);
          return false;
        }
        }
        return false;
      };
      getItems()
  }, []);

  if (isLoading) {
    return <Loading />; // Ou un composant de chargement personnalisé
  }




  
  const liens = ['Accueil', 'À Propos', 'Services', 'Contact'];

  function countCharacters(description: any) {
    if (description) {
        console.log("ee" , description.length)
        return description.length;
    }
    return 0;
}


const handleClick = (e: any, id = null) => {
    if(e.target.id === "modifier" && id !== null) {
        router.push(`/modifier?id=${id}`);
    } else {
        setShowDialog(true); // Set state to show dialog
    }
};


  return (
    <>    
        <Navbar links={liens} />

            <section id="annonces" className='m-10  hidden md:block '>
              <div className='w-full flex justify-center p-15'>
                  <h3 className='text-[45px] font-bold'>Mes annonces publiées</h3>
              </div>
                    {
                    data.map((element: any, index: any) => (
                        <div key={index} className="flex w-[80%] max-w-1475 mx-auto mt-20">
                        <div className="card flex justify-between items-center">
                            <div>
                                {
                                    element.img1 ? 
                                    
                                    <img className="w-[200px] h-[200px]" src={element.img1} alt="" />
                                    
                                    :
                                    <div className="flex">

                                    <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" strokeWidth={1.5} stroke="currentColor" className="w-6 h-6">
                                        <path strokeLinecap="round" strokeLinejoin="round" d="m2.25 15.75 5.159-5.159a2.25 2.25 0 0 1 3.182 0l5.159 5.159m-1.5-1.5 1.409-1.409a2.25 2.25 0 0 1 3.182 0l2.909 2.909m-18 3.75h16.5a1.5 1.5 0 0 0 1.5-1.5V6a1.5 1.5 0 0 0-1.5-1.5H3.75A1.5 1.5 0 0 0 2.25 6v12a1.5 1.5 0 0 0 1.5 1.5Zm10.5-11.25h.008v.008h-.008V8.25Zm.375 0a.375.375 0 1 1-.75 0 .375.375 0 0 1 .75 0Z" />
                                    </svg>
                                    <p className="pl-[10px]">
                                        aucune photo ...
                                        </p>
                                    </div>
    
                                }
                         

                            </div>
                            <div className="description-annonce block flex flex-col h-[80%] pl-[10px] text-[18px]">

                            <div className="status bg-[#ac3328]">{element.type_of_service == 'R' ? 'Recherche' : 'Cessation' }</div>
                            <div className="font-bold py-2">
                                {element.title} <br /><span className="price"></span>
                            </div>
                            <div className='pt-[20px]'>
                                <p className="">
                                    {
                                        countCharacters(element.description) > 100 ?
                                        <span>{element.description.slice(0, 50) + ' ...'}</span>
                                        :
                                        <i>{element.description}</i>
                                    }
                                
                                </p>
                                Date de publication : {format(new Date(element.created_at), 'dd/MM/yyyy HH:mm')} <br />
                                {
                                element.dis.map((dis, disIndex) => (
                                    <p key={disIndex}> Lieu de prestation : {dis.place_of_service}</p>
                                ))
                                }
                            </div>
                            </div>
                            <div className='flex py-10 gap-4'>
                            <button type="button" id="modifier"  onClick={(e) => handleClick(e, element.service_id)} className="bg-[#f59f4d] text-white ">Modifier</button>
                            <button type="button" id="supprimer" onClick={(e)=>handleClickOpen()} className="bg-[#f59f4d] text-white">Supprimer</button>
             
                            {showDialog && 
                            
                            <Fragment>
             
                            <Dialog
                              open={open}
                              onClose={handleClose}
                              aria-labelledby="alert-dialog-title"
                              aria-describedby="alert-dialog-description"
                            >
                              <DialogTitle id="alert-dialog-title">
                                {"Use Google's location service?"}
                              </DialogTitle>
                              <DialogContent>
                                <DialogContentText id="alert-dialog-description">
                                  Let Google help apps determine location. This means sending anonymous
                                  location data to Google, even when no apps are running.
                                </DialogContentText>
                              </DialogContent>
                              <DialogActions>
                                <Button onClick={handleClose}>Disagree</Button>
                                <Button onClick={handleClose} autoFocus>
                                  Agree
                                </Button>
                              </DialogActions>
                            </Dialog>
                            </Fragment>
                            }
                            </div>
                        </div>
                        </div>
                    ))
                    }
            </section>

            <section id='annonce-mobile' className='m-10 block md:hidden'>
            <div className='text-title'>
                  <h3 className='font-bold'>Mes annonces publiées</h3>
              </div>
                <div   className="container card-flex">
                  <div  className="card with-card">
                    <div  className="flex">
                      <img  src="/assets/flowers-little.png" alt="" />
                        <div  className="flex button-container">
                          <div >
                            <button   className="big primary" tabIndex={0}> Recherche </button>
                            </div>
                            </div>
                            <div  className="center-side column">
                              <div  className="mariage-part">
                                <p  className="bold mariage-title">Location de salle</p>
                                <p  className="bold red-color mariage-price">500 €</p>
                                </div>
                                <div  className="date-part">
                                  <div >
                                    <p  className="grey-color">15/05/2021</p>
                                    <p  className="grey-color">Vitry-sur-Seine</p>
                                    </div>
                              </div>
                              </div>
                              </div>
                              </div>
                <div  className="card with-card">
                  <div  className="flex">
                    <img  src="/assets/flowers-little.png" alt="" />
                      <div  className="flex button-container">
                        <div >
                          <button   className="big primary" tabIndex={0} ng-reflect-router-link="/guingui/list"> Recherche </button>
                          </div></div><div  className="center-side column">
                            <div  className="mariage-part">
                              <p  className="bold mariage-title">Location de salle</p>
                              <p  className="bold red-color mariage-price">500 €</p>
                              </div><div  className="date-part">
                                <div ><p  className="grey-color">15/05/2021</p>
                                <p  className="grey-color">Vitry-sur-Seine</p>
                                </div>
                            </div>
                          </div>
                      </div>
                      </div>
                      </div>
            </section>


    </>
  )
}

