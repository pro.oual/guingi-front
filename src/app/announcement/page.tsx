'use client'
import { useEffect, useState } from "react";
import Loading from "./loading";
import Navbar from "@/src/components/base/navbar/navbar";
import "./style.css"
import { format } from "date-fns";


export default function AnnouncementPage() {
 const [data, setData] = useState<any>([]);
 const [loading, setLoading] = useState(true);
  const liens = ['Accueil', 'À Propos', 'Services', 'Contact'];
    
  
  
  
    useEffect(()=>{
    let isMounted = true;
    const getItems = async () => {
      const searchParams = new URLSearchParams(window.location.search);
      let service;
      if (searchParams.get('service')) {
        service = searchParams.get('service')?.toString();
      }
      try {
        const url = `http://localhost:3000/v1/service/informations/${service}`;
          const response = await fetch(url, {
            method: 'GET',
            headers: {
            },
          });
          if (!response.ok) {
            return false;
          }
          const fetchedData  = await response.json();
          if (isMounted) {
            setData(fetchedData );
            setLoading(false);
          }
      } catch (error) {
        console.error(error);
        setLoading(false);
      }
      return false;
    };
    getItems();
    return () => {
        isMounted = false;
      };
    },[]);

    if (loading) {
        return <Loading />;
    }
  return (
    <>    
    <Navbar links={liens} />

    <div className="max-w-2xl mx-auto p-6 bg-white shadow-md rounded-md my-10">
    <div className="flex justify-between mt-8">
      <a href={`/dashboard/chat/conversation?recipient=${data.element.user.user_id}`} className="bg-blue-500 text-white py-2 px-4 rounded-md hover:bg-blue-600">
        Ecrire
      </a>

        <button className="bg-gray-500 text-white py-2 px-4 rounded-md hover:bg-gray-600">{data.element.type_of_service === 'R' ? 'Recherche' : 'Cessation'}</button>
      </div>

      <div className="flex gap-4 w-[20%]">

      {data.element.img1 && <img src={data.element.img1} alt="Announcement Image" className="mt-8 max-w-full rounded-md shadow-md" />}
      {data.element.img2 && <img src={data.element.img2} alt="Announcement Image" className="mt-8 max-w-full rounded-md shadow-md" />}
      {data.element.img3 && <img src={data.element.img3} alt="Announcement Image" className="mt-8 max-w-full rounded-md shadow-md" />}
      {data.element.img4 && <img src={data.element.img4} alt="Announcement Image" className="mt-8 max-w-full rounded-md shadow-md" />}

      </div>
      <h1 className="text-4xl font-bold text-blue-500">{data.element.title}</h1>
      <p className="text-gray-700 my-4">{data.element.description}</p>

      {data.element.type_of_service === 'R' ? 
        <>
        </> 
      
      : 
      
      <p className="text-lg text-gray-800">     Price: {data.element.new_price} €</p>
      
      }


      {data.otcs ? 
        <div className="mt-6">
        <h2 className="text-2xl font-semibold text-gray-800">Options :</h2>
        <ul>
        <div className="bg-gray-100 p-4 my-4 rounded-md">
        {data.otcs.map((otcs: any) => (
            <li key={'li'+otcs.OptionTypeCategorieService_id}>{otcs.name}</li>
        ))}
          </div>
        </ul>
      </div>
      : 
      <></>
       }
    



      <div className="mt-6">
        <h2 className="text-2xl font-semibold text-gray-800">FAQ:</h2>
        {data.qa.map((qaItem: any) => (
          <div key={qaItem.questionAnswer_id} className="bg-gray-100 p-4 my-4 rounded-md">
            <p className="text-gray-700">Questions: {qaItem.question.join(', ')}</p>
            <p className="text-gray-700">Reponses: {qaItem.answer.join(', ')}</p>
          </div>
        ))}
      </div>

      <div className="mt-6">
        <h2 className="text-2xl font-semibold text-gray-800">Informations:</h2>
        {data.dis.map((disItem: any) => (
          <div key={disItem.DurationInfoService_id} className="bg-gray-100 p-4 my-4 rounded-md">
            <p className="text-gray-700">Date de l`&apos;`evenement: {format(new Date(disItem.datetime_of_start), 'dd/MM/yyyy HH:mm')}</p>
            <p className="text-gray-700">Localisation: {disItem.place_of_service}</p>
            {/* Add more details as needed */}
          </div>
        ))}
      </div>


    </div>


    </>
  )
}