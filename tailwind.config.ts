import type { Config } from 'tailwindcss'

const config: Config = {
  content: [
    './src/pages/**/*.{js,ts,jsx,tsx,mdx}',
    './src/components/**/*.{js,ts,jsx,tsx,mdx}',
    './src/app/**/*.{js,ts,jsx,tsx,mdx}',
  ],
  theme: {
    extend: {
      backgroundImage:{
        'slide1': "url('/assets/slide1.jpg')",
        'slide2': "url('/assets/slide2.jpg')",
        'slide3': "url('/assets/slide3.jpg')",
      },
      animation: {
        'slide-header': 'imageAnimation 12s linear infinite 0s',
      },
      keyframes: {
        imageAnimation: {
        '0%': { 
          'transition-timing-function': 'ease-in',
          'opacity': '0',
        },
        '8%': {
          'transition-timing-function': 'ease-out',
        'opacity': '1',
        },
        '17%': { 
          'opacity': '1',
          },
        '25%': { 
        'opacity': '0', 
        },
        '100%': { 
        'opacity': '0', 
        },
        }
      }
    },
  },
  plugins: [

    require('@tailwindcss/aspect-ratio'),
  ],
}
export default config
